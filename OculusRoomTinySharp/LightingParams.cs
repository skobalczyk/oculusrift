using System.Runtime.InteropServices;
using SharpDX;

namespace OculusRoomTinySharp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct LightingParams
    {
        public Color3 Ambient;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)] 
        public Vector3[] LightPos;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public Vector3[] LightColor;

        public uint LightCount;

        public uint Version;

        public void Update(Matrix view, Vector3[] sceneLightPos)
        {
            Version++;
            for (int i = 0; i < LightCount; i++)
            {
                LightPos[i] = (Vector3)Vector3.Transform(sceneLightPos[i], view);
            }
        }
    }
}