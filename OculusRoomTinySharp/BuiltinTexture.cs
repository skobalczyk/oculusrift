namespace OculusRoomTinySharp
{
    public enum BuiltinTexture
    {
        None,
        Checker,
        Block,
        Panel
    };
}