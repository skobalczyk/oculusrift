using SharpDX;
using SharpDX.DXGI;
using SharpDX.Toolkit.Graphics;

namespace OculusRoomTinySharp
{
    public struct Vertex
    {
        [VertexElement("Position", Format.R32G32B32_Float)] 
        public Vector3 Position;

        [VertexElement("Color", Format.R8G8B8A8_UNorm)]
        public Color Color;

        [VertexElement("TexCoord", Format.R32G32B32_Float)] 
        public Vector2 TexCoord;

        [VertexElement("Normal", Format.R32G32B32_Float)] 
        public Vector3 Normal;

        public Vertex(Vector3 position, Color color, Vector2 texCoord, Vector3 normal)
        {
            Position = position;
            Color = color;
            TexCoord = texCoord;
            Normal = normal;
        }
    }
}