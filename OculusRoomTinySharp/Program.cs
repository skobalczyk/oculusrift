﻿using System;
using System.Windows.Forms;
using SharpDX.Windows;

namespace OculusRoomTinySharp
{
    public class Program
    {
        private static int mouseX;

        [STAThread]
        public static void Main()
        {
            var game = new OculusRoomTinyGame();

            var form = new RenderForm("SharpDX - OculusRoomTinySharp Sample");
            form.KeyDown += (s,e)=>game.OnKey(true, e.KeyCode);
            form.KeyUp += (s, e) => game.OnKey(false, e.KeyCode);

            form.MouseDown += (s, e) =>
            {
                if (e.Button == MouseButtons.Left)
                    mouseX = e.X;
            };
            form.MouseMove += (s, e) =>
            {
                if (e.Button == MouseButtons.Left)
                {
                    var deltaX = e.X - mouseX;
                    mouseX = e.X;
                    game.OnMouseMove(deltaX, 0);
                }
            };

            game.IsMouseVisible = true;
            game.Run(form);
        }

        
    }
}
