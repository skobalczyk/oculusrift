using System.Diagnostics;
using System.Linq;

using SharpDX;

namespace OculusRoomTinySharp
{
    using SharpDX.Toolkit.Content;
    using SharpDX.Toolkit.Graphics;

    public class RenderDevice
    {
        private readonly GraphicsDevice _graphicsDevice;

        private Effect _effect;
        private VertexInputLayout _modelVertexIL;
        private Matrix _proj;

        public RenderDevice(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
        }

        public GraphicsDevice GraphicsDevice
        {
            get { return _graphicsDevice; }
        }

        public void Load(IContentManager content)
        {
            _effect = content.Load<Effect>("Shader");

            _modelVertexIL = VertexInputLayout.New<Vertex>(0);
        }

        public void SetLighting(LightingParams lighting)
        {
            if (_effect.Parameters["Ambient"] == null) return;

            _effect.Parameters["Ambient"].SetValue(lighting.Ambient);
            _effect.Parameters["LightPos"].SetValue(lighting.LightPos);
            _effect.Parameters["LightColor"].SetValue(lighting.LightColor);
            _effect.Parameters["LightCount"].SetValue(lighting.LightCount);
        }

        public void Render(Matrix view, Model model)
        {
            if (model.VertexBuffer == null)
            {
                var vb = Buffer.Vertex.New(_graphicsDevice, model.Vertices.ToArray());
                model.VertexBuffer = vb;
            }
            if (model.IndexBuffer == null)
            {
                var ib = Buffer.Index.New(_graphicsDevice, model.Indices.ToArray());
                model.IndexBuffer = ib;
            }

            Render(model.Fill, model.VertexBuffer, model.IndexBuffer, Utilities.SizeOf<Vertex>(),
                view, 0, model.Indices.Count, model.PrimitiveType, false);
        }


        public void SetProjection(Matrix proj)
        {
            _proj = proj;
        }

        private void Render(ShaderFill fill, Buffer vertices, Buffer indices, int stride,
            Matrix view, int offset, int count, PrimitiveType rprim, bool updateUniformData)
        {
            Debug.Assert(fill != null, "fill != null");

            var inputLayout = (fill != null && fill.InputLayout != null) 
                ? fill.InputLayout 
                : _modelVertexIL;

            _graphicsDevice.SetVertexInputLayout(inputLayout);

            if (indices != null)
            {
                _graphicsDevice.SetIndexBuffer(indices, false);
            }

            _graphicsDevice.SetVertexBuffer(0, vertices, stride, offset);

            /*ShaderBase* vshader = ((ShaderBase*)shaders->GetShader(Shader_Vertex));
            unsigned char* vertexData = vshader->UniformData;
            if (vertexData)
            {
		        // TODO: some VSes don't start with StandardUniformData!
		        if ( updateUniformData )
		        {
			        StandardUniformData* stdUniforms = (StandardUniformData*) vertexData;
			        stdUniforms->View = view.Transposed();
			        stdUniforms->Proj = StdUniforms.Proj;
		        }
		        UniformBuffers[Shader_Vertex]->Data(Buffer_Uniform, vertexData, vshader->UniformsSize);
		        vshader->SetUniformBuffer(UniformBuffers[Shader_Vertex]);
            }

            for(int i = Shader_Vertex + 1; i < Shader_Count; i++)
                if (shaders->GetShader(i))
                {
                    ((ShaderBase*)shaders->GetShader(i))->UpdateBuffer(UniformBuffers[i]);
                    ((ShaderBase*)shaders->GetShader(i))->SetUniformBuffer(UniformBuffers[i]);
                }

                fill.Set(rprim);*/

            var v = view;
            v.Transpose();
            _effect.Parameters["View"].SetValue(v);

            var p = _proj;
        //    p.Transpose();
            _effect.Parameters["Proj"].SetValue(p);
            
            _effect.Parameters["gColor"].SetValue(new Color(0));


            if (fill.Textures != null && fill.Textures.Length > 0)
            {
               _effect.Parameters["Texture"].SetResource(fill.Textures[0]);
            }
            //_effect.Techniques["Solid"].Passes[0].Apply();
            //_effect.Techniques["Gouraud"].Passes[0].Apply();
            //_effect.Techniques["LitSolid"].Passes[0].Apply();

            _effect.Techniques[fill.ShaderName].Passes[0].Apply();

            if (indices != null)
            {
                _effect.Parameters["Linear"].SetResource(_graphicsDevice.SamplerStates.AnisotropicWrap);
                _graphicsDevice.DrawIndexed(rprim, count, 0, 0);
            }
            else
            {
                _graphicsDevice.Draw(rprim, count, 0);
            }
        }
    }
}