namespace OculusRoomTinySharp
{
    public class SlabModel
    {
        public Slab[] Slabs { get; set; }
        public BuiltinTexture Tex;

        public SlabModel(Slab[] slabs, BuiltinTexture tex = BuiltinTexture.None)
        {
            Slabs = slabs;
            Tex = tex;
        }
    }
}