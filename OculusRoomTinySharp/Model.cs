using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using SharpDX;
using SharpDX.Toolkit.Graphics;

namespace OculusRoomTinySharp
{
    public class Model : Node
    {
        private readonly List<Vertex> _vertices;
        private readonly IReadOnlyCollection<Vertex> _verticesReadOnly;

        private readonly List<ushort> _indices;
        private readonly IReadOnlyCollection<ushort> _indicesReadOnly;

        public ShaderFill Fill;
        public bool Visible;

        public PrimitiveType PrimitiveType { get; private set; }

        internal Buffer VertexBuffer;
        internal Buffer IndexBuffer;

        public Model(PrimitiveType primType)
        {
            PrimitiveType = primType;
            Fill = null; //TODO:
            Visible = true;

            _vertices = new List<Vertex>();
            _verticesReadOnly = new ReadOnlyCollection<Vertex>(_vertices);

            _indices = new List<ushort>();
            _indicesReadOnly = new ReadOnlyCollection<ushort>(_indices);
        }

        public override NodeType Type
        {
            get { return NodeType.Model; }
        }

        public IReadOnlyCollection<Vertex> Vertices
        {
            get { return _verticesReadOnly; }
        }

        public IReadOnlyCollection<ushort> Indices
        {
            get { return _indicesReadOnly; }
        }

        // Returns the index next added vertex will have.
        public ushort GetNextVertexIndex()
        {
            return (ushort) _vertices.Count;
        }

        public ushort AddVertex(Vertex v)
        {
            Debug.Assert(VertexBuffer == null && IndexBuffer == null);
            ushort index = (ushort)_vertices.Count;
            _vertices.Add(v);
            return index;
        }

        public void AddTriangle(ushort a, ushort b, ushort c)
        {
            _indices.Add(a);
            _indices.Add(b);
            _indices.Add(c);
        }

        // Uses texture coordinates for uniform world scaling (must use a repeat sampler).
        public void AddSolidColorBox(Vector3 p1, Vector3 p2, Color c)
        {
            AddSolidColorBox(p1.X, p1.Y, p1.Z, p2.X, p2.Y, p2.Z, c);
        }

        public void AddSolidColorBox(float x1, float y1, float z1, float x2, float y2, float z2, Color c)
        {
            float t;

            if (x1 > x2)
            {
                t = x1;
                x1 = x2;
                x2 = t;
            }
            if (y1 > y2)
            {
                t = y1;
                y1 = y2;
                y2 = t;
            }
            if (z1 > z2)
            {
                t = z1;
                z1 = z2;
                z2 = t;
            }


            // Cube vertices and their normals.
            Vertex[] cubeVertices =
            {
                new Vertex(new Vector3(x1, y2, z1), c, new Vector2(z1, x1), new Vector3(0.0f, 1.0f, 0.0f)),
                new Vertex(new Vector3(x2, y2, z1), c, new Vector2(z1, x2), new Vector3(0.0f, 1.0f, 0.0f)),
                new Vertex(new Vector3(x2, y2, z2), c, new Vector2(z2, x2), new Vector3(0.0f, 1.0f, 0.0f)),
                new Vertex(new Vector3(x1, y2, z2), c, new Vector2(z2, x1), new Vector3(0.0f, 1.0f, 0.0f)),

                new Vertex(new Vector3(x1, y1, z1), c, new Vector2(z1, x1), new Vector3(0.0f, -1.0f, 0.0f)),
                new Vertex(new Vector3(x2, y1, z1), c, new Vector2(z1, x2), new Vector3(0.0f, -1.0f, 0.0f)),
                new Vertex(new Vector3(x2, y1, z2), c, new Vector2(z2, x2), new Vector3(0.0f, -1.0f, 0.0f)),
                new Vertex(new Vector3(x1, y1, z2), c, new Vector2(z2, x1), new Vector3(0.0f, -1.0f, 0.0f)),

                new Vertex(new Vector3(x1, y1, z2), c, new Vector2(z2, y1), new Vector3(-1.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(x1, y1, z1), c, new Vector2(z1, y1), new Vector3(-1.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(x1, y2, z1), c, new Vector2(z1, y2), new Vector3(-1.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(x1, y2, z2), c, new Vector2(z2, y2), new Vector3(-1.0f, 0.0f, 0.0f)),

                new Vertex(new Vector3(x2, y1, z2), c, new Vector2(z2, y1), new Vector3(1.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(x2, y1, z1), c, new Vector2(z1, y1), new Vector3(1.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(x2, y2, z1), c, new Vector2(z1, y2), new Vector3(1.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(x2, y2, z2), c, new Vector2(z2, y2), new Vector3(1.0f, 0.0f, 0.0f)),

                new Vertex(new Vector3(x1, y1, z1), c, new Vector2(x1, y1), new Vector3(0.0f, 0.0f, -1.0f)),
                new Vertex(new Vector3(x2, y1, z1), c, new Vector2(x2, y1), new Vector3(0.0f, 0.0f, -1.0f)),
                new Vertex(new Vector3(x2, y2, z1), c, new Vector2(x2, y2), new Vector3(0.0f, 0.0f, -1.0f)),
                new Vertex(new Vector3(x1, y2, z1), c, new Vector2(x1, y2), new Vector3(0.0f, 0.0f, -1.0f)),

                new Vertex(new Vector3(x1, y1, z2), c, new Vector2(x1, y1), new Vector3(0.0f, 0.0f, 1.0f)),
                new Vertex(new Vector3(x2, y1, z2), c, new Vector2(x2, y1), new Vector3(0.0f, 0.0f, 1.0f)),
                new Vertex(new Vector3(x2, y2, z2), c, new Vector2(x2, y2), new Vector3(0.0f, 0.0f, 1.0f)),
                new Vertex(new Vector3(x1, y2, z2), c, new Vector2(x1, y2), new Vector3(0.0f, 0.0f, 1.0f))
            };

            ushort startIndex = GetNextVertexIndex();

            foreach (Vertex v in cubeVertices)
            {
                AddVertex(v);
            }

            ushort[] cubeIndices =
            {
                0, 1, 3,
                3, 1, 2,

                5, 4, 6,
                6, 4, 7,

                8, 9, 11,
                11, 9, 10,

                13, 12, 14,
                14, 12, 15,

                16, 17, 19,
                19, 17, 18,

                21, 20, 22,
                22, 20, 23
            };

            // Renumber indices
            for (int i = 0; i < cubeIndices.Length/3; i++)
            {
                AddTriangle((ushort) (cubeIndices[i*3] + startIndex),
                    (ushort) (cubeIndices[i*3 + 1] + startIndex),
                    (ushort) (cubeIndices[i*3 + 2] + startIndex));
            }
        }

        public override void Render(Matrix ltw, RenderDevice ren)
        {
            if (Visible)
            {
                Matrix m = GetMatrix()*ltw;
                ren.Render(m, this);
            }
        }

    }
}