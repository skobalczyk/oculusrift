using SharpDX.Toolkit.Graphics;

namespace OculusRoomTinySharp
{
    public class ShaderFill
    {
        public ShaderFill(string shaderName)
        {
            ShaderName = shaderName;
        }

        public string ShaderName { get; set; }

        public Texture2D[] Textures { get; set; }
        public VertexInputLayout InputLayout { get; set; }
    }
}