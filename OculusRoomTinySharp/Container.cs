using System.Collections.Generic;
using System.Collections.ObjectModel;
using SharpDX;

namespace OculusRoomTinySharp
{
    /// <summary>
    /// Container stores a collection of rendering nodes (Models or other containers).
    /// </summary>
    public class Container : Node
    {
        private readonly List<Node> _nodes;
        private readonly IReadOnlyCollection<Node> _readOnlyNodes;

        public IReadOnlyCollection<Node> Nodes
        {
            get { return _readOnlyNodes; }
        }

        public Container()
        {
            _nodes = new List<Node>();
            _readOnlyNodes = new ReadOnlyCollection<Node>(_nodes);
        }

        public void Add(Node n)
        {
            _nodes.Add(n);
        }

        public void Clear()
        {
            _nodes.Clear();
        }

        public override NodeType Type
        {
            get { return NodeType.Container; }
        }

        public override void Render(Matrix ltw, RenderDevice ren)
        {
            Matrix m = GetMatrix() * ltw;
            foreach (Node n in _nodes)
            {
                n.Render(m, ren);
            }
        }
    };
   
}