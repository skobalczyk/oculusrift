using SharpDX;
using SharpDX.Direct3D11;

namespace OculusRoomTinySharp
{
    using SharpDX.Toolkit.Graphics;

    internal class FillCollection
    {
        private readonly ShaderFill _litSolid;
        private readonly ShaderFill[] _litTextures = new ShaderFill[3];

        public FillCollection(RenderDevice render)
        {
            var builtinTextures = new Texture2D[4];

            // Create floor checkerboard texture.
            {
                Color[] checker = new Color[256 * 256];
                for (int j = 0; j < 256; j++)
                    for (int i = 0; i < 256; i++)
                    {
                        checker[j*256 + i] = (((i/4 >> 5) ^ (j/4 >> 5)) & 1) != 0
                            ? new Color(180, 180, 180, 255)
                            : new Color(80, 80, 80, 255);
                    }

                builtinTextures[(int)BuiltinTexture.Checker] = CreateTexture(render, checker);
            }

            // Ceiling panel texture.
            {
                Color[] panel = new Color[256*256];
                for (int j = 0; j < 256; j++)
                    for (int i = 0; i < 256; i++)
                        panel[j*256 + i] = (i/4 == 0 || j/4 == 0)
                            ? new Color(80, 80, 80, 255)
                            : new Color(180, 180, 180, 255);

                builtinTextures[(int) BuiltinTexture.Panel] = CreateTexture(render, panel);
            }

            // Wall brick textures.
            {
                Color[] block = new Color[256*256];
                for (int j = 0; j < 256; j++)
                    for (int i = 0; i < 256; i++)
                        block[j*256 + i] = (((j/4 & 15) == 0) ||
                                            (((i/4 & 15) == 0) && ((((i/4 & 31) == 0) ^ ((j/4 >> 4) & 1) != 0) == false)))
                            ? new Color(60, 60, 60, 255)
                            : new Color(180, 180, 180, 255);
                
                builtinTextures[(int)BuiltinTexture.Block] = CreateTexture(render, block);
            }

            _litSolid = new ShaderFill("LitSolid");

            _litTextures = new ShaderFill[4];
            for (int i = 0; i < 4; i++)
            {
                LitTextures[i] = new ShaderFill("LitTexture");
                LitTextures[i].Textures = new[] {builtinTextures[i]};
            }
        }

        private static Texture2D CreateTexture(RenderDevice render, Color[] checker)
        {
            // create source texture
            Texture2D texture = Texture2D.New(render.GraphicsDevice, 256, 256, PixelFormat.R8G8B8A8.UNorm, checker);

            // create texture with mip-maps
            Texture2D texturewithmips = Texture2D.New(render.GraphicsDevice, texture.Width, texture.Height, MipMapCount.Auto,
                texture.Format, TextureFlags.ShaderResource | TextureFlags.RenderTarget, 1, ResourceUsage.Default);

            // copy and generate mips
            render.GraphicsDevice.Copy(texture, 0, texturewithmips, 0);
            texturewithmips.GenerateMipMaps(render.GraphicsDevice);

            // free source
            texture.Dispose();

            return texturewithmips;
        }

        public ShaderFill LitSolid
        {
            get { return _litSolid; }
        }

        public ShaderFill[] LitTextures
        {
            get { return _litTextures; }
        }
    }
}