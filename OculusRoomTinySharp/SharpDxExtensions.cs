using OculusSharp.CAPI;
using SharpDX;

namespace OculusRoomTinySharp
{
    public static class SharpDxExtensions
    {

        public static Matrix ToMatrix(this Matrix4f m)
        {
            return new Matrix(m.M00, m.M01, m.M02, m.M03,
                m.M10, m.M11, m.M12, m.M13,
                m.M20, m.M21, m.M22, m.M23,
                m.M30, m.M31, m.M32, m.M33);
        }

        public static Quaternion ToQuaternion(this Quatf q)
        {
            return new Quaternion(q.z, q.y, q.z, q.w);
        }

        public static Vector3 ToVector3(this Vector3f v)
        {
            return  new Vector3(v.x, v.y, v.z);
        }
    }
}