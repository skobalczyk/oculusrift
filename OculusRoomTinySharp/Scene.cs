using System.Diagnostics;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace OculusRoomTinySharp
{
    public class Scene : GameSystem
    {
        private static readonly Slab[] FloorSlabs =
        {
            // Floor
            new Slab(new Vector3(-10.0f, -0.1f, -20.0f), new Vector3(10.0f, 0.0f, 20.1f), new Color(0.5f, 0.5f, 0.5f))
        };

        public static readonly SlabModel Floor = new SlabModel(FloorSlabs, BuiltinTexture.Checker);

        private static readonly Slab[] CeilingSlabs =
        {
            new Slab(new Vector3(-10.0f, 4.0f, -20.0f), new Vector3(10.0f, 4.1f, 20.1f), new Color(0.5f, 0.5f, 0.5f))
        };

        public static readonly SlabModel Ceiling = new SlabModel(CeilingSlabs, BuiltinTexture.Panel);

        private static readonly Slab[] RoomSlabs =
        {
            // Left Wall
            new Slab(new Vector3(-10.1f, 0.0f, -20.0f), new Vector3(-10.0f, 4.0f, 20.0f), new Color(0.5f, 0.5f, 0.5f)),
            // Back Wall
            new Slab(new Vector3(-10.0f, -0.1f, -20.1f), new Vector3(10.0f, 4.0f, -20.0f), new Color(0.5f, 0.5f, 0.5f)),
            // Right Wall
            new Slab(new Vector3(10.0f, -0.1f, -20.0f), new Vector3(10.1f, 4.0f, 20.0f), new Color(0.5f, 0.5f, 0.5f))
        };

        public static readonly SlabModel Room = new SlabModel(RoomSlabs, BuiltinTexture.Block);

        private static readonly Slab[] FixtureSlabs =
        {
            // Right side shelf
            new Slab(new Vector3(9.5f, 0.75f, 3.0f), new Vector3(10.1f, 2.5f, 3.1f), new Color(128, 128, 128)),
            // Verticals
            new Slab(new Vector3(9.5f, 0.95f, 3.7f), new Vector3(10.1f, 2.75f, 3.8f), new Color(128, 128, 128)),
            new Slab(new Vector3(9.5f, 1.20f, 2.5f), new Vector3(10.1f, 1.30f, 3.8f), new Color(128, 128, 128)),
            // Horizontals
            new Slab(new Vector3(9.5f, 2.00f, 3.0f), new Vector3(10.1f, 2.10f, 4.2f), new Color(128, 128, 128)),

            // Right railing    
            new Slab(new Vector3(5.0f, 1.1f, 20.0f), new Vector3(10.0f, 1.2f, 20.1f), new Color(128, 128, 128)),
            // Bars
            new Slab(new Vector3(9.0f, 1.1f, 20.0f), new Vector3(9.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(8.0f, 1.1f, 20.0f), new Vector3(8.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(7.0f, 1.1f, 20.0f), new Vector3(7.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(6.0f, 1.1f, 20.0f), new Vector3(6.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(5.0f, 1.1f, 20.0f), new Vector3(5.1f, 0.0f, 20.1f), new Color(128, 128, 128)),

            // Left railing    
            new Slab(new Vector3(-10.0f, 1.1f, 20.0f), new Vector3(-5.0f, 1.2f, 20.1f), new Color(128, 128, 128)),
            // Bars
            new Slab(new Vector3(-9.0f, 1.1f, 20.0f), new Vector3(-9.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(-8.0f, 1.1f, 20.0f), new Vector3(-8.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(-7.0f, 1.1f, 20.0f), new Vector3(-7.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(-6.0f, 1.1f, 20.0f), new Vector3(-6.1f, 0.0f, 20.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(-5.0f, 1.1f, 20.0f), new Vector3(-5.1f, 0.0f, 20.1f), new Color(128, 128, 128)),

            // Bottom Floor 2
            new Slab(new Vector3(-15.0f, -6.1f, 18.0f), new Vector3(15.0f, -6.0f, 30.0f), new Color(128, 128, 128))
        };

        public static readonly SlabModel Fixtures = new SlabModel(FixtureSlabs);


        private static readonly Slab[] FurnitureSlabs =
        {
            // Table
            new Slab(new Vector3(-1.8f, 0.7f, 1.0f), new Vector3(0.0f, 0.8f, 0.0f), new Color(128, 128, 88)),
            new Slab(new Vector3(-1.8f, 0.7f, 0.0f), new Vector3(-1.8f + 0.1f, 0.0f, 0.0f + 0.1f), new Color(128, 128, 88)), // Leg 1
            new Slab(new Vector3(-1.8f, 0.7f, 1.0f), new Vector3(-1.8f + 0.1f, 0.0f, 1.0f - 0.1f), new Color(128, 128, 88)), // Leg 2
            new Slab(new Vector3(0.0f, 0.7f, 1.0f), new Vector3(0.0f - 0.1f, 0.0f, 1.0f - 0.1f), new Color(128, 128, 88)), // Leg 3
            new Slab(new Vector3(0.0f, 0.7f, 0.0f), new Vector3(0.0f - 0.1f, 0.0f, 0.0f + 0.1f), new Color(128, 128, 88)), // Leg 4

            // Chair
            new Slab(new Vector3(-1.4f, 0.5f, -1.1f), new Vector3(-0.8f, 0.55f, -0.5f), new Color(88, 88, 128)), // Set
            new Slab(new Vector3(-1.4f, 1.0f, -1.1f), new Vector3(-1.4f + 0.06f, 0.0f, -1.1f + 0.06f), new Color(88, 88, 128)), // Leg 1
            new Slab(new Vector3(-1.4f, 0.5f, -0.5f), new Vector3(-1.4f + 0.06f, 0.0f, -0.5f - 0.06f), new Color(88, 88, 128)), // Leg 2
            new Slab(new Vector3(-0.8f, 0.5f, -0.5f), new Vector3(-0.8f - 0.06f, 0.0f, -0.5f - 0.06f), new Color(88, 88, 128)), // Leg 2
            new Slab(new Vector3(-0.8f, 1.0f, -1.1f), new Vector3(-0.8f - 0.06f, 0.0f, -1.1f + 0.06f), new Color(88, 88, 128)), // Leg 2
            new Slab(new Vector3(-1.4f, 0.97f, -1.05f), new Vector3(-0.8f, 0.92f, -1.10f), new Color(88, 88, 128)) // Back high bar
        };

        private static readonly SlabModel Furniture = new SlabModel(FurnitureSlabs);

        private static readonly Slab[] PostsSlabs =
        {
            // Posts
            new Slab(new Vector3(0, 0.0f, 0.0f), new Vector3(0.1f, 1.3f, 0.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 0.4f), new Vector3(0.1f, 1.3f, 0.5f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 0.8f), new Vector3(0.1f, 1.3f, 0.9f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 1.2f), new Vector3(0.1f, 1.3f, 1.3f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 1.6f), new Vector3(0.1f, 1.3f, 1.7f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 2.0f), new Vector3(0.1f, 1.3f, 2.1f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 2.4f), new Vector3(0.1f, 1.3f, 2.5f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 2.8f), new Vector3(0.1f, 1.3f, 2.9f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 3.2f), new Vector3(0.1f, 1.3f, 3.3f), new Color(128, 128, 128)),
            new Slab(new Vector3(0, 0.0f, 3.6f), new Vector3(0.1f, 1.3f, 3.7f), new Color(128, 128, 128)),
        };

        private static readonly SlabModel Posts = new SlabModel(PostsSlabs);

        private static Model CreateModel(Vector3 pos, SlabModel sm, FillCollection fills)
        {
            Model m = new Model(PrimitiveType.TriangleList);
            m.Position = pos;

            foreach (Slab s in sm.Slabs)
            {
                m.AddSolidColorBox(s.Point1, s.Point2, s.Color);
            }

            if (sm.Tex > BuiltinTexture.None)
                m.Fill = fills.LitTextures[(int) sm.Tex];
            else
                m.Fill = fills.LitSolid;
            
            return m;
        }

        public static void PopulateRoomScene(Scene scene, RenderDevice render)
        {
            var fills = new FillCollection(render);

            scene.World.Add(CreateModel(new Vector3(0, 0, 0), Room, fills));
            scene.World.Add(CreateModel(new Vector3(0, 0, 0), Floor, fills));
            scene.World.Add(CreateModel(new Vector3(0, 0, 0), Ceiling, fills));
            scene.World.Add(CreateModel(new Vector3(0, 0, 0), Fixtures, fills));
            scene.World.Add(CreateModel(new Vector3(0, 0, 0), Furniture, fills));
            scene.World.Add(CreateModel(new Vector3(0, 0, 4), Furniture, fills));
            scene.World.Add(CreateModel(new Vector3(-3, 0, 3), Posts, fills));

            scene.SetAmbient(new Color3(0.65f, 0.65f, 0.65f));
            scene.AddLight(new Vector3(-2, 4, -2), new Vector3(8f, 8f, 8f));
            scene.AddLight(new Vector3(3, 4, -3), new Vector3(2f, 1f, 1f));
            scene.AddLight(new Vector3(-4, 3, 25), new Vector3(3f, 6f, 3f));
        }

        private LightingParams _lighting;

        private readonly Vector3[] _lightPos;
        private readonly Container _world;

        public Container World
        {
            get { return _world; }
        }

        public Scene(Game game)
            : base(game)
        {
            game.GameSystems.Add(this);

            _world = new Container();

            _lighting.LightPos = new Vector3[8];
            _lighting.LightColor = new Vector3[8];
            _lightPos = new Vector3[8];
        }

        private void SetAmbient(Color3 color)
        {
            _lighting.Ambient = color;
        }

        private void AddLight(Vector3 pos, Vector3 color)
        {
            uint n = _lighting.LightCount;
            Debug.Assert(n < 8);

            _lightPos[n] = pos;
            _lighting.LightColor[n] = color;
            _lighting.LightCount++;
        }

        public void Clear()
        {
            World.Clear();

            _lighting.Ambient = new Color3(0.0f, 0.0f, 0.0f);
            _lighting.LightCount = 0;
        }

        public void Render(RenderDevice ren, Matrix view)
        {
            _lighting.Update(view, _lightPos);

            ren.SetLighting(_lighting);

            World.Render(view, ren);
        }
    }
}