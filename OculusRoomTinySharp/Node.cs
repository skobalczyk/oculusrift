using SharpDX;

namespace OculusRoomTinySharp
{
    public enum NodeType
    {
        NonDisplay,
        Container,
        Model
    };

    public abstract class Node
    {
        private Vector3 _position;
        private Matrix? _matrix;
        private Quaternion _orientation = Quaternion.Identity;

        public abstract NodeType Type { get; }

        public Vector3 Position
        {
            get { return _position; }
            set
            {
                if (_position == value) return;
                _position = value;
                _matrix = null;
            }
        }

        public Quaternion Orientation
        {
            get { return _orientation; }
            set
            {
                if (_orientation == value) return;
                _orientation = value;
                _matrix = null;
            }
        }

        public void Move(Vector3 p)
        {
            Position += p;
        }

        public void Rotate(Quaternion q)
        {
            Orientation = q*Orientation;
        }

        public Matrix GetMatrix()
        {
            if (_matrix == null)
            {
                _matrix = Matrix.Translation(Position) *
                    Matrix.RotationQuaternion(Orientation);
            }
            return _matrix.Value;
        }

        public abstract void Render(Matrix ltw, RenderDevice ren);
    }
}