﻿cbuffer Global : register(b0)
{
	float4x4 Proj;
	float4x4 View;
	float4 gColor;
}

Texture2D Texture : register(t0);
SamplerState Linear : register(s0);

struct Varyings
{
   float4 Position : SV_Position;
   float4 Color    : COLOR0;
   float2 TexCoord : TEXCOORD0;
   float3 Normal   : NORMAL;
   float3 VPos     : TEXCOORD4;
};

void StdVertexShader(in float4 Position : POSITION, in float4 Color : COLOR0, in float2 TexCoord : TEXCOORD0,
          in float3 Normal : NORMAL,
          out Varyings ov)
{
   ov.Position = mul(Proj, mul(View, Position));
   ov.Normal = mul(View, float4(Normal,0)).xyz;
   ov.VPos = mul(View, Position).xyz;
   ov.TexCoord = TexCoord;
   ov.Color = Color;
}

void DirectVertexShader(in float4 Position : POSITION, in float4 Color : COLOR0,
          in float2 TexCoord : TEXCOORD0, in float3 Normal : NORMAL,
          out float4 oPosition : SV_Position, out float4 oColor : COLOR,
          out float2 oTexCoord : TEXCOORD0,
          out float3 oNormal : NORMAL)
{
   oPosition = mul(View, Position);
   oTexCoord = TexCoord;
   oColor = Color;
   oNormal = mul(View, float4(Normal, 0)).xyz;
};


float4 SolidPixelShader(in Varyings ov) : SV_Target
{
   return gColor;
};

float4 GouraudPixelShader(in Varyings ov) : SV_Target
{
   return ov.Color;
};

float4 TexturePixelShader(in Varyings ov) : SV_Target
{
   float4 color2 = ov.Color * Texture.Sample(Linear, ov.TexCoord);
   if (color2.a <= 0.4)
		discard;
   return color2;
}

cbuffer Lighting : register(b1)
{
    float3 Ambient;
    float3 LightPos[8];
    float3 LightColor[8];
    uint  LightCount;
};


float4 DoLight(Varyings v)
{
   float3 norm = normalize(v.Normal);
   float3 light = Ambient.rgb;
   for (uint i = 0; i < LightCount; i++)
   {
	   float3 ltp = (LightPos[i].xyz - v.VPos);
       float  ldist = dot(ltp,ltp);
       ltp = normalize(ltp);
       light += saturate(LightColor[i].rgb * v.Color.rgb * dot(norm, ltp) / sqrt(ldist));
   }
   return float4(light.rgb, v.Color.a);
}

/*float4 DoLight(Varyings v)\n"              \
    "{\n"                                       \
    "   float3 norm = normalize(v.Normal);\n"   \
    "   float3 light = Ambient;\n"              \
    "   for (uint i = 0; i < LightCount; i++)\n"\
    "   {\n"                                        \
    "       float3 ltp = (LightPos[i] - v.VPos);\n" \
    "       float  ldist = dot(ltp,ltp);\n"         \
    "       ltp = normalize(ltp);\n"                \
    "       light += saturate(LightColor[i] * v.Color.rgb * dot(norm, ltp) / sqrt(ldist));\n"\
    "   }\n"                                        \
    "   return float4(light, v.Color.a);\n"         \
    "}\n"*/

float4 LitSolidPixelShader(in Varyings ov) : SV_Target
{
   return DoLight(ov) * ov.Color;
};

float4 LitTexturePixelShader(in Varyings ov) : SV_Target
{
   return DoLight(ov) * Texture.Sample(Linear, ov.TexCoord);
};


technique Solid
{
	pass P0
	{
		Profile = 10.0;
		VertexShader = StdVertexShader;
		PixelShader = SolidPixelShader;
		GeometryShader = NULL;
	}
}

technique Gouraud
{
	pass P0
	{
		Profile = 10.0;
		VertexShader = StdVertexShader;
		PixelShader = GouraudPixelShader;
		GeometryShader = NULL;
	}
}

technique Textured
{
	pass P0
	{
		Profile = 10.0;
		VertexShader = StdVertexShader;
		PixelShader = TexturePixelShader;
		GeometryShader = NULL;
	}
}

technique LitSolid
{
	pass P0
	{
		Profile = 10.0;
		VertexShader = StdVertexShader;
		PixelShader = LitSolidPixelShader;
		GeometryShader = NULL;
	}
}

technique LitTexture
{
	pass P0
	{
		Profile = 10.0;
		VertexShader = StdVertexShader;
		PixelShader = LitTexturePixelShader;
		GeometryShader = NULL;
	}
}