using SharpDX;

namespace OculusRoomTinySharp
{
    public class Slab
    {
        public Slab(Vector3 p1, Vector3 p2, Color c)
        {
            Point1 = p1;
            Point2 = p2;
            Color = c;
        }

        public Vector3 Point1;
        public Vector3 Point2;
        public Color Color;
    }
}