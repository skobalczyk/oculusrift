using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OculusSharp.CAPI;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Windows;
using Buffer = SharpDX.Toolkit.Graphics.Buffer;
using Rectangle = System.Drawing.Rectangle;

namespace OculusRoomTinySharp
{
    internal class OculusRoomTinyGame : Game
    {
         private readonly GraphicsDeviceManager _graphicsDeviceManager;

        private readonly EyeRenderDesc[] _eyeRenderDesc = new EyeRenderDesc[2];
        private readonly Recti[] _eyeRenderViewport = new Recti[2];
        
        private RenderTarget2D _rendertargetTexture;
        private DepthStencilBuffer _rendertargetDepthStencil;

        private Hmd _hmd;

#if SDK_RENDER
//#define OVR_D3D_VERSION 11
        private D3D11TextureData[] EyeTexture = new D3D11TextureData[2];
#else
        private Effect _distortionShader;
    	private VertexInputLayout _distortionMeshVertexInputLayout;
        readonly Buffer[]        _distortionMeshVBs = new Buffer[2];
        readonly Buffer[]        _distortionMeshIBs = new Buffer[2];
        readonly Vector2f[][] UVScaleOffset = { new Vector2f[2], new Vector2f[2] };
#endif

        private Scene _roomScene;
        private RenderDevice _ren;

        private BasicEffect _basicEffect;
        private GeometricPrimitive _teapot;
        private SharpDX.Toolkit.Graphics.Model _skullModel;

        private float _bodyYaw;
        private Vector3 _headPos;

        public OculusRoomTinyGame()
        {
            // Creates a graphics manager. This is mandatory.
            _graphicsDeviceManager = new GraphicsDeviceManager(this);

#if DEBUG
            // Enable debug device
            _graphicsDeviceManager.DeviceCreationFlags = DeviceCreationFlags.Debug;
#endif

            // Setup the relative directory to the executable directory
            // for loading contents with the ContentManager
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            // Initialize HMD
            Hmd.Initialize();

            if (Hmd.DetectHmd() > 0)
                // use 1st device
                _hmd = new Hmd(0);
            else
                // use debug device
                _hmd = new Hmd(HmdType.Hmd_DK2);

            HmdDesc hmdDesc = _hmd.GetDesc();
            if (hmdDesc.ProductName[0] == '\0')
            {
                //MessageBoxA(NULL, "Rift detected, display not enabled.", "", MB_OK);
            }


            _ren = new RenderDevice(GraphicsDevice);

            //Setup Window and Graphics - use window frame if relying on Oculus driver
            /*const int backBufferMultisample = 1;
            bool UseAppWindowFrame = (HMD->HmdCaps & ovrHmdCap_ExtendDesktop) ? false : true;
            HWND window = Util_InitWindowAndGraphics(Recti(hmdDesc.WindowsPos, hmdDesc.Resolution),
                                         FullScreen, backBufferMultisample, UseAppWindowFrame, &pRender);
            if (!window) return 1;*/

            // get current window handle
            var window = ((Form)this.Window.NativeWindow).Handle;
            _hmd.AttachToWindow(window, null, null);

            //Configure Stereo settings.
            Sizei recommenedTex0Size = _hmd.GetFovTextureSize(EyeType.Eye_Left, hmdDesc.DefaultEyeFov[0], 1.0f);
            Sizei recommenedTex1Size = _hmd.GetFovTextureSize(EyeType.Eye_Right, hmdDesc.DefaultEyeFov[1], 1.0f);

            Sizei renderTargetSize;
            renderTargetSize.w = recommenedTex0Size.w + recommenedTex1Size.w;
            renderTargetSize.h = Math.Max(recommenedTex0Size.h, recommenedTex1Size.h);

            //TODO: const int eyeRenderMultisample = 1;
            _rendertargetTexture = ToDispose(RenderTarget2D.New(GraphicsDevice, renderTargetSize.w, renderTargetSize.h, PixelFormat.R8G8B8A8.UNorm));
            _rendertargetDepthStencil = ToDispose(DepthStencilBuffer.New(GraphicsDevice, renderTargetSize.w, renderTargetSize.h, DepthFormat.Depth24Stencil8));

            // The actual RT size may be different due to HW limits.
            renderTargetSize.w = _rendertargetTexture.Width;
            renderTargetSize.h = _rendertargetTexture.Height;

            // Initialize eye rendering information.
            // The viewport sizes are re-computed in case RenderTargetSize changed due to HW limitations.
            var eyeFov = new List<FovPort> {hmdDesc.DefaultEyeFov[0], hmdDesc.DefaultEyeFov[1]};

            _eyeRenderViewport[0] = new Recti
            {
                Pos = new Vector2i {x = 0, y = 0},
                Size = new Sizei {w = renderTargetSize.w/2, h = renderTargetSize.h}
            };
            _eyeRenderViewport[1] = new Recti
            {
                Pos = new Vector2i {x = (renderTargetSize.w + 1)/2, y = 0},
                Size = _eyeRenderViewport[0].Size
            };

#if SDK_RENDER

        	// Query D3D texture data.
            EyeTexture = new D3D11TextureData[2];
            EyeTexture[0].Header.API            = RenderAPIType.RenderAPI_D3D11;
            EyeTexture[0].Header.TextureSize    = RenderTargetSize;
            EyeTexture[0].Header.RenderViewport = EyeRenderViewport[0];
            EyeTexture[0].pTexture              = ((SharpDX.Direct3D11.Texture2D)_rendertargetTexture).NativePointer;
            EyeTexture[0].pSRView               = rendertargetTextureSRV.NativePointer;

            // Right eye uses the same texture, but different rendering viewport.
            EyeTexture[1] = EyeTexture[0];
            EyeTexture[1].Header.RenderViewport = EyeRenderViewport[1];

            // Configure d3d11.
            D3D11ConfigData d3d11cfg = new D3D11ConfigData();
            d3d11cfg.Header.API         = RenderAPIType.RenderAPI_D3D11;
            d3d11cfg.Header.RTSize      = RenderTargetSize; // new Sizei(HMD->Resolution.w, HMD->Resolution.h);
            d3d11cfg.Header.Multisample = 0; //backBufferMultisample;
            d3d11cfg.pDevice            = ((SharpDX.Direct3D11.Device)GraphicsDevice).NativePointer;
            d3d11cfg.pDeviceContext     = ((SharpDX.Direct3D11.DeviceContext)GraphicsDevice).NativePointer;
            d3d11cfg.pBackBufferRT      = ((RenderTargetView) GraphicsDevice.BackBuffer).NativePointer;
            d3d11cfg.pSwapChain         = ((SwapChain)GraphicsDevice.Presenter.NativePresenter).NativePointer;

            if (!_hmd.ConfigureRendering(d3d11cfg,
		                                   DistortionCap.DistortionCap_Chromatic | DistortionCap.DistortionCap_Vignette |
                                           DistortionCap.DistortionCap_TimeWarp | DistortionCap.DistortionCap_Overdrive,
								           eyeFov, EyeRenderDesc))	
                return ;
#else
            _distortionMeshVertexInputLayout = VertexInputLayout.New(0, new[]
            {
                new VertexElement("Position", 0, Format.R32G32_Float),
                new VertexElement("Position", 1, Format.R32_Float),
                new VertexElement("Position", 2, Format.R32_Float),
                new VertexElement("TexCoord", 0, Format.R32G32_Float),
                new VertexElement("TexCoord", 1, Format.R32G32_Float),
                new VertexElement("TexCoord", 2, Format.R32G32_Float)
            });

            for ( int eyeNum = 0; eyeNum < 2; eyeNum++ )
            {
                // Allocate mesh vertices, registering with renderer using the OVR vertex format.
                var meshData = new DistortionMesh();
                _hmd.CreateDistortionMesh((EyeType) eyeNum, eyeFov[eyeNum], 
                    DistortionCap.DistortionCap_Chromatic | DistortionCap.DistortionCap_TimeWarp, meshData);

                _distortionMeshVBs[eyeNum] = ToDispose(Buffer.Vertex.New(GraphicsDevice, meshData.VertexData.ToArray()));
                _distortionMeshIBs[eyeNum] = ToDispose(Buffer.Index.New(GraphicsDevice, meshData.IndexData.ToArray()));

		        //Create eye render description for use later
		        _eyeRenderDesc[eyeNum] = _hmd.GetRenderDesc((EyeType) eyeNum,  eyeFov[eyeNum]);

		        //Do scale and offset
                var offsetOut = new List<Vector2f>(2);
                _hmd.GetRenderScaleAndOffset(eyeFov[eyeNum], renderTargetSize, _eyeRenderViewport[eyeNum], offsetOut);
                UVScaleOffset[eyeNum] = offsetOut.ToArray();
            }
#endif

            _hmd.SetEnabledCaps(HmdCap.HmdCap_LowPersistence | HmdCap.HmdCap_DynamicPrediction);

	        // Start the sensor which informs of the Rift's pose and motion
            _hmd.ConfigureTracking(TrackingCap.TrackingCap_Orientation |
                                  TrackingCap.TrackingCap_MagYawCorrection | 
                                  TrackingCap.TrackingCap_Position, 0);

            // This creates lights and models.
            _roomScene = new Scene(this);
            Scene.PopulateRoomScene(_roomScene, _ren);

            // Adjust eye position and rotation from controls, maintaining y position from HMD.
            _bodyYaw = 3.141592f;
            _headPos = new Vector3(0.0f, 1.6f, -5.0f);
            _headPos.Y = _hmd.GetFloat("EyeHeight", _headPos.Y);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            _ren.Load(Content);

#if !SDK_RENDER
            _distortionShader = ToDisposeContent(Content.Load<Effect>("DistortionShader"));
#endif

            _basicEffect = ToDisposeContent(new BasicEffect(GraphicsDevice));
            _basicEffect.EnableDefaultLighting();
            _basicEffect.DiffuseColor = (Vector4) Color.Orange;

            _teapot = ToDisposeContent(GeometricPrimitive.Teapot.New(GraphicsDevice, 0.4f));
            _skullModel = ToDisposeContent(Content.Load<SharpDX.Toolkit.Graphics.Model>("Skull"));
            BasicEffect.EnableDefaultLighting(_skullModel);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

#if SDK_RENDER
            _hmd.BeginFrame(0);
#else
            _hmd.BeginFrameTiming(0);
            // Retrieve data useful for handling the Health and Safety Warning - unused, but here for reference
            var hswDisplayState = new HSWDisplayState();
            _hmd.GetHSWDisplayState(ref hswDisplayState);
#endif

            // get eye pose
            Posef[] eyeRenderPose = new Posef[2];
            for (int eyeIndex = 0; eyeIndex < 2; eyeIndex++)
            {
                eyeRenderPose[eyeIndex] = _hmd.GetEyePose((EyeType)eyeIndex);
            }

            bool freezeEyeRender = RespondToControls(ref _bodyYaw, ref _headPos, eyeRenderPose[1].Orientation.ToQuaternion(), gameTime.ElapsedGameTime);

            GraphicsDevice.SetRenderTargets(_rendertargetDepthStencil, _rendertargetTexture);
            GraphicsDevice.SetViewport(0, 0, _rendertargetTexture.Width, _rendertargetTexture.Height);
            GraphicsDevice.Clear(Color.Black);

            GraphicsDevice.SetRasterizerState(GraphicsDevice.RasterizerStates.CullBack);
            GraphicsDevice.SetDepthStencilState(GraphicsDevice.DepthStencilStates.Default);

            for (int eyeIndex = 0; eyeIndex < 2; eyeIndex++)
            {
                //TODO: ovrEyeType eye = _hmd.EyeRenderOrder[eyeIndex];
                var eye = (EyeType) eyeIndex;

                // Get view and projection matrices
                Matrix rollPitchYaw = Matrix.RotationY(_bodyYaw);
                Matrix finalRollPitchYaw = rollPitchYaw * Matrix.RotationQuaternion(eyeRenderPose[(int)eye].Orientation.ToQuaternion());
                Vector3 finalUp = (Vector3) Vector3.Transform(new Vector3(0, 1, 0), finalRollPitchYaw);
                Vector3 finalForward = (Vector3) Vector3.Transform(new Vector3(0, 0, -1), finalRollPitchYaw);

                var eyePos = eyeRenderPose[(int) eye].Position;
                Vector3 shiftedEyePos = _headPos + (Vector3)Vector3.Transform(eyePos.ToVector3(), rollPitchYaw);

                Matrix view = Matrix.LookAtRH(shiftedEyePos, shiftedEyePos + finalForward, finalUp);

                Matrix proj = Hmd.Matrix4f_Projection(_eyeRenderDesc[(int)eye].Fov, 0.01f, 10000.0f, true).ToMatrix();

            //   GraphicsDevice.SetRasterizerState(GraphicsDevice.RasterizerStates.WireFrameCullNone);

               // view = Matrix.LookAtRH(new Vector3(-5,1.7f,-8), new Vector3(0,1,0), new Vector3(0,1,0));
               // proj = Matrix.PerspectiveFovRH(MathUtil.DegreesToRadians(45f), GraphicsDevice.Viewport.AspectRatio, 0.01f, 10000.0f);

                var v = _eyeRenderViewport[(int) eye];
                GraphicsDevice.SetViewport(v.Pos.x, v.Pos.y, v.Size.w, v.Size.h);

                _ren.SetProjection(proj);

                view = view * Matrix.Translation(_eyeRenderDesc[(int)eye].ViewAdjust.ToVector3());
                _roomScene.Render(_ren, view);

                // draw teapot
                proj.Transpose();
                _basicEffect.Projection = proj;
                _basicEffect.View = view;
                
                _basicEffect.World = Matrix.Translation(-0.9f, 0.9f, 0.5f);
                _teapot.Draw(_basicEffect);

                // draw skull
                _skullModel.Draw(GraphicsDevice, Matrix.Scaling(0.06f)*Matrix.RotationY(MathUtil.Pi)*Matrix.Translation(-0.9f, 0.8f, 4.5f), view, proj);
            }

          //  GraphicsDevice.SetRenderTargets((RenderTargetView)null);


#if SDK_RENDER
            _hmd.EndFrame(eyeRenderPose, EyeTexture);
#else
            GraphicsDevice.SetRasterizerState(GraphicsDevice.RasterizerStates.CullBack);

	        // set default render targets
            GraphicsDevice.SetRenderTargets(GraphicsDevice.Presenter.DepthStencilBuffer, GraphicsDevice.Presenter.BackBuffer);
            GraphicsDevice.SetViewport(GraphicsDevice.Viewport);

            // Clear screen
            GraphicsDevice.Clear(new Color4(0));

        	// Setup shader
        	_distortionShader.Parameters["Texture"].SetResource(_rendertargetTexture);
            GraphicsDevice.SetVertexInputLayout(_distortionMeshVertexInputLayout);

	        for(int eyeNum = 0; eyeNum < 2; eyeNum++)
	        {
		        // Get and set shader constants
                _distortionShader.Parameters["EyeToSourceUVScale"].SetValue(UVScaleOffset[eyeNum][0]);
                _distortionShader.Parameters["EyeToSourceUVOffset"].SetValue(UVScaleOffset[eyeNum][1]);

	            List<Matrix4f> timeWarpMatrices = new List<Matrix4f>(2);
                _hmd.GetEyeTimewarpMatrices((EyeType)eyeNum, eyeRenderPose[eyeNum], timeWarpMatrices);
	            var m = timeWarpMatrices[0].ToMatrix(); m.Transpose();
                _distortionShader.Parameters["EyeRotationStart"].SetValue(m);  //Nb transposed when set
                m = timeWarpMatrices[1].ToMatrix(); m.Transpose();
                _distortionShader.Parameters["EyeRotationEnd"].SetValue(m);  //Nb transposed when set

                // set sampler
                _distortionShader.Parameters["Linear"].SetResource(GraphicsDevice.SamplerStates.LinearClamp);

                // Perform distortion
                GraphicsDevice.SetVertexBuffer(0, _distortionMeshVBs[eyeNum], Utilities.SizeOf<DistortionVertex>());
                GraphicsDevice.SetIndexBuffer(_distortionMeshIBs[eyeNum], false);

                _distortionShader.Techniques[0].Passes[0].Apply();


                GraphicsDevice.DrawIndexed(PrimitiveType.TriangleList, _distortionMeshIBs[eyeNum].ElementCount);
            
                _distortionShader.Techniques[0].Passes[0].UnApply();
            }

	       // pRender->SetDefaultRenderTarget();
	        //pRender->Present( true ); // Vsync enabled

            // Only flush GPU for ExtendDesktop; not needed in Direct App Renering with Oculus driver.
            //TODO: if ((_hmd.GetEnabledCaps() & HmdCap.HmdCap_ExtendDesktop) != 0)
		   //     pRender->WaitUntilGpuIdle();  
	        
            _hmd.EndFrameTiming();
#endif
        }

        private bool ShiftDown;
        private bool ControlDown;
        private bool FreezeEyeRender;

        private bool MoveForward;
        private bool MoveBack;
        private bool MoveLeft;
        private bool MoveRight;

        private float AdditionalYawFromMouse;

        public void OnKey(bool down, Keys vk)
        {
            if (down && _hmd != null)
                _hmd.DismissHSWDisplay();

            switch (vk)
            {
                case Keys.Q: if (down && ControlDown) Exit(); break;
                case Keys.Escape: if (!down) Exit(); break;

                case Keys.R: if (down && _hmd != null) _hmd.RecenterPose(); break;

                case Keys.W: MoveForward = down; break;
                case Keys.S: MoveBack = down; break;
                case Keys.A: MoveLeft = down; break;
                case Keys.D: MoveRight = down; break;

                case Keys.Up : MoveForward = down; break;
                case Keys.Down: MoveBack = down; break;

                case Keys.F: FreezeEyeRender = !down ? !FreezeEyeRender : FreezeEyeRender; break;

                case Keys.F11: if (!down) ToggleFullScreen(!GraphicsDevice.Presenter.IsFullScreen); break;

                case Keys.Shift: ShiftDown = down; break;
                case Keys.Control: ControlDown = down; break;
            }
        }

        // Movement speed, in m/s applied during keyboard motion.
        public void OnMouseMove(int x, int y)
        {
            AdditionalYawFromMouse -= (Sensitivity * x) / 360.0f;
        }

        const float MoveSpeed = 3.0f;
        const float Sensitivity = 1.0f;

        private bool RespondToControls(ref float eyeYaw, ref Vector3 eyePos, Quaternion poseOrientation, TimeSpan elapsedGameTime)
        {
/*
        #if 0//Optional debug output
	        char debugString[1000];
	        sprintf_s(debugString,"Pos = (%0.2f, %0.2f, %0.2f)\n",EyePos.x,EyePos.y,EyePos.z);
	        OutputDebugStringA(debugString);
	        #endif
*/

            //Mouse rotation
            eyeYaw += AdditionalYawFromMouse;
            AdditionalYawFromMouse = 0;

            //Get HeadYaw
            float tempHeadPitch, tempHeadRoll, headYaw;

            //Y, X, Z
            ToEuler(poseOrientation, out headYaw, out tempHeadPitch, out tempHeadRoll);

            //Move on Eye pos from controls
            Vector3 localMoveVector = new Vector3(0, 0, 0);
            Matrix yawRotate = Matrix.RotationY(eyeYaw + headYaw);

            if (MoveForward) localMoveVector += new Vector3(0, 0, -1);
            if (MoveBack) localMoveVector += new Vector3(0, 0, +1);
            if (MoveRight) localMoveVector += new Vector3(1, 0, 0);
            if (MoveLeft) localMoveVector += new Vector3(-1, 0, 0);

            Vector3 orientationVector = Vector3.TransformCoordinate(localMoveVector, yawRotate);

            //const float deltaTime = 1.0f/60.0f;
            float deltaTime = (float) elapsedGameTime.TotalSeconds;
            orientationVector *= MoveSpeed*deltaTime*(ShiftDown ? 3.0f : 1.0f);
            eyePos += orientationVector;

            //Some rudimentary limitation of movement, so not to go through walls
            const float minDistanceToWall = 0.30f;
            eyePos.X = Math.Max(eyePos.X, -10.0f + minDistanceToWall);
            eyePos.X = Math.Min(eyePos.X, 10.0f - minDistanceToWall);
            eyePos.Z = Math.Max(eyePos.Z, -20.0f + minDistanceToWall);

            //Return if need to freeze or not
            return (FreezeEyeRender);
        }

        private void ToEuler(Quaternion q, out float yaw, out float pitch, out float roll)
        {
            float ww = q.W * q.W;
            float xx = q.X * q.X;
            float yy = q.Y * q.Y;
            float zz = q.Z * q.Z;
            
            const float Singularity = 0.499f;
            float lengthSqd = xx + yy + zz + ww;
            float singularityTest = q.Y * q.W - q.X * q.Z;
            float singularityValue = Singularity * lengthSqd;

            if (singularityTest > singularityValue)
            {
                yaw = (float) (-2*Math.Atan2(q.Z, q.W));
                pitch = 90.0f;
                roll = 0.0f;
                return;
            }

            if (singularityTest < -singularityValue) 
            {
                yaw = (float) (2*Math.Atan2(q.Z, q.W));
                pitch = -90.0f;
                roll = 0.0f;
                return;
            };

            yaw = (float) Math.Atan2(2.0f*(q.Y*q.Z + q.X*q.W), 1.0f - 2.0f*(xx + yy));
            pitch = (float) Math.Asin(2.0f*singularityTest/lengthSqd);
            roll = (float) Math.Atan2(2.0f*(q.X*q.Y + q.Z*q.W), 1.0f - 2.0f*(yy + zz));
        }

        private Rectangle _windowBounds;

        private void ToggleFullScreen(bool fullScreen)
        {
            if (fullScreen)
            {
                RenderForm form = (RenderForm)this.Window.NativeWindow;
                _windowBounds = form.Bounds;
                Screen screen = Screen.FromControl(form);
                GraphicsDevice.Presenter.PrefferedFullScreenOutputIndex = Array.IndexOf(Screen.AllScreens, screen);
                GraphicsDevice.Presenter.Resize(screen.Bounds.Width, screen.Bounds.Height, Format.R8G8B8A8_UNorm);
                GraphicsDevice.Presenter.PresentInterval = PresentInterval.One;
                GraphicsDevice.Presenter.IsFullScreen = true;                 
            }
            else
            {
                Form form = ((Form) this.Window.NativeWindow);
                GraphicsDevice.Presenter.Resize(_windowBounds.Width, _windowBounds.Height, Format.R8G8B8A8_UNorm);
                GraphicsDevice.Presenter.PresentInterval = PresentInterval.Default;
                GraphicsDevice.Presenter.IsFullScreen = false;
            }
        }
    }
}