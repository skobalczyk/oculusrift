// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using System.Diagnostics;
using System.Collections.Generic;
using OculusSharp.CAPI;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;
using MapFlags = SharpDX.Direct3D11.MapFlags;

namespace MiniCubeTexure
{
    /// <summary>
    /// SharpDX MiniCubeTexture Direct3D 11 Sample
    /// </summary>
    internal static class Program
    {
        private static Hmd hmd = null;
        private static Sizei renderTargetSize;
        private static OculusSharp.CAPI.HmdDesc hmdDesc;
        private static Texture2D renderTargetTexture;
        private static RenderTargetView renderTargetView;
        private static ShaderResourceView renderTargetSRV;

        [STAThread]
        private static void Main()
        {
            var form = new RenderForm("SharpDX - MiniCubeTexture OculusSharp Sample");
            form.KeyPress += form_KeyPress;

            Hmd.Initialize();

            if (Hmd.DetectHmd() > 0)
                hmd = new Hmd(0);
            else
                hmd = new Hmd(OculusSharp.CAPI.HmdType.Hmd_DK2);
            hmdDesc = hmd.GetDesc();

            hmd.ConfigureTracking(OculusSharp.CAPI.TrackingCap.TrackingCap_Orientation | OculusSharp.CAPI.TrackingCap.TrackingCap_MagYawCorrection | OculusSharp.CAPI.TrackingCap.TrackingCap_Position, 0);

            Sizei recommenedTex0Size = hmd.GetFovTextureSize(EyeType.Eye_Left, hmdDesc.DefaultEyeFov[0], 0.5f);
            Sizei recommenedTex1Size = hmd.GetFovTextureSize(EyeType.Eye_Right, hmdDesc.DefaultEyeFov[1], 0.5f);

            renderTargetSize.w = recommenedTex0Size.w + recommenedTex1Size.w;
            renderTargetSize.h = Math.Max(recommenedTex0Size.h, recommenedTex1Size.h);

            form.ClientSize = new System.Drawing.Size(renderTargetSize.w, renderTargetSize.h);

            // SwapChain description
            var desc = new SwapChainDescription()
            {
                BufferCount = 1,
                ModeDescription =
                    new ModeDescription(form.ClientSize.Width, form.ClientSize.Height,
                                        new Rational(60, 1), Format.R8G8B8A8_UNorm),
                IsWindowed = true,
                OutputHandle = form.Handle,
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };

            // Create Device and SwapChain
            Device device;
            SwapChain swapChain;
            Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.Debug, desc, out device, out swapChain);
            var context = device.ImmediateContext;

            // Ignore all windows events
            var factory = swapChain.GetParent<Factory>();
            factory.MakeWindowAssociation(form.Handle, WindowAssociationFlags.IgnoreAll);

            // New RenderTargetView from the backbuffer
            var backBuffer = Texture2D.FromSwapChain<Texture2D>(swapChain, 0);
            var renderView = new RenderTargetView(device, backBuffer);

            renderTargetTexture = new Texture2D(device, new Texture2DDescription()
            {
                Format = Format.R8G8B8A8_UNorm,
                ArraySize = 1,
                MipLevels = 1,
                Width = renderTargetSize.w,
                Height = renderTargetSize.h,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None
            });
            renderTargetView = new RenderTargetView(device, renderTargetTexture);
            renderTargetSRV = new ShaderResourceView(device, renderTargetTexture);

            // Compile Vertex and Pixel shaders
            var vertexShaderByteCode = ShaderBytecode.CompileFromFile("MiniCubeTexture.fx", "VS", "vs_4_0");
            var vertexShader = new VertexShader(device, vertexShaderByteCode);

            var pixelShaderByteCode = ShaderBytecode.CompileFromFile("MiniCubeTexture.fx", "PS", "ps_4_0");
            var pixelShader = new PixelShader(device, pixelShaderByteCode);

            // Layout from VertexShader input signature
            var layout = new InputLayout(device, ShaderSignature.GetInputSignature(vertexShaderByteCode), new[]
                    {
                        new InputElement("POSITION", 0, Format.R32G32B32A32_Float, 0, 0),
                        new InputElement("TEXCOORD", 0, Format.R32G32_Float, 16, 0)
                    });

            // Instantiate Vertex buiffer from vertex data
            var vertices = Buffer.Create(device, BindFlags.VertexBuffer, new[]
                                  {
                                      // 3D coordinates              UV Texture coordinates
                                      -1.0f, -1.0f, -1.0f, 1.0f,     0.0f, 1.0f, // Front
                                      -1.0f,  1.0f, -1.0f, 1.0f,     0.0f, 0.0f,
                                       1.0f,  1.0f, -1.0f, 1.0f,     1.0f, 0.0f,
                                      -1.0f, -1.0f, -1.0f, 1.0f,     0.0f, 1.0f,
                                       1.0f,  1.0f, -1.0f, 1.0f,     1.0f, 0.0f,
                                       1.0f, -1.0f, -1.0f, 1.0f,     1.0f, 1.0f,

                                      -1.0f, -1.0f,  1.0f, 1.0f,     1.0f, 0.0f, // BACK
                                       1.0f,  1.0f,  1.0f, 1.0f,     0.0f, 1.0f,
                                      -1.0f,  1.0f,  1.0f, 1.0f,     1.0f, 1.0f,
                                      -1.0f, -1.0f,  1.0f, 1.0f,     1.0f, 0.0f,
                                       1.0f, -1.0f,  1.0f, 1.0f,     0.0f, 0.0f,
                                       1.0f,  1.0f,  1.0f, 1.0f,     0.0f, 1.0f,

                                      -1.0f, 1.0f, -1.0f,  1.0f,     0.0f, 1.0f, // Top
                                      -1.0f, 1.0f,  1.0f,  1.0f,     0.0f, 0.0f,
                                       1.0f, 1.0f,  1.0f,  1.0f,     1.0f, 0.0f,
                                      -1.0f, 1.0f, -1.0f,  1.0f,     0.0f, 1.0f,
                                       1.0f, 1.0f,  1.0f,  1.0f,     1.0f, 0.0f,
                                       1.0f, 1.0f, -1.0f,  1.0f,     1.0f, 1.0f,

                                      -1.0f,-1.0f, -1.0f,  1.0f,     1.0f, 0.0f, // Bottom
                                       1.0f,-1.0f,  1.0f,  1.0f,     0.0f, 1.0f,
                                      -1.0f,-1.0f,  1.0f,  1.0f,     1.0f, 1.0f,
                                      -1.0f,-1.0f, -1.0f,  1.0f,     1.0f, 0.0f,
                                       1.0f,-1.0f, -1.0f,  1.0f,     0.0f, 0.0f,
                                       1.0f,-1.0f,  1.0f,  1.0f,     0.0f, 1.0f,

                                      -1.0f, -1.0f, -1.0f, 1.0f,     0.0f, 1.0f, // Left
                                      -1.0f, -1.0f,  1.0f, 1.0f,     0.0f, 0.0f,
                                      -1.0f,  1.0f,  1.0f, 1.0f,     1.0f, 0.0f,
                                      -1.0f, -1.0f, -1.0f, 1.0f,     0.0f, 1.0f,
                                      -1.0f,  1.0f,  1.0f, 1.0f,     1.0f, 0.0f,
                                      -1.0f,  1.0f, -1.0f, 1.0f,     1.0f, 1.0f,

                                       1.0f, -1.0f, -1.0f, 1.0f,     1.0f, 0.0f, // Right
                                       1.0f,  1.0f,  1.0f, 1.0f,     0.0f, 1.0f,
                                       1.0f, -1.0f,  1.0f, 1.0f,     1.0f, 1.0f,
                                       1.0f, -1.0f, -1.0f, 1.0f,     1.0f, 0.0f,
                                       1.0f,  1.0f, -1.0f, 1.0f,     0.0f, 0.0f,
                                       1.0f,  1.0f,  1.0f, 1.0f,     0.0f, 1.0f,
                            });

            // Create Constant Buffer
            var contantBuffer = new Buffer(device, Utilities.SizeOf<Matrix>(), ResourceUsage.Default, BindFlags.ConstantBuffer, CpuAccessFlags.None, ResourceOptionFlags.None, 0);


            // Create Depth Buffer & View
            var depthBuffer = new Texture2D(device, new Texture2DDescription()
            {
                Format = Format.D32_Float_S8X24_UInt,
                ArraySize = 1,
                MipLevels = 1,
                Width = form.ClientSize.Width,
                Height = form.ClientSize.Height,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.DepthStencil,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None
            });



            var depthView = new DepthStencilView(device, depthBuffer);

            // Load texture and create sampler
            var texture = Texture2D.FromFile<Texture2D>(device, "GeneticaMortarlessBlocks.jpg");
            var textureView = new ShaderResourceView(device, texture);

            var sampler = new SamplerState(device, new SamplerStateDescription()
            {
                Filter = Filter.MinMagMipLinear,
                AddressU = TextureAddressMode.Wrap,
                AddressV = TextureAddressMode.Wrap,
                AddressW = TextureAddressMode.Wrap,
                BorderColor = Color.Black,
                ComparisonFunction = Comparison.Never,
                MaximumAnisotropy = 16,
                MipLodBias = 0,
                MinimumLod = 0,
                MaximumLod = 16,
            });


            D3D11ConfigData config = new D3D11ConfigData();
            config.Header.API = RenderAPIType.RenderAPI_D3D11;
            config.Header.Multisample = 1;
            config.Header.RTSize = renderTargetSize;
            config.pDevice = device.NativePointer;
            config.pDeviceContext = context.NativePointer;
            config.pSwapChain = swapChain.NativePointer;
            config.pBackBufferRT = renderView.NativePointer;


            List<FovPort> fovList = new List<FovPort>();
            List<EyeRenderDesc> eyeList = new List<EyeRenderDesc>();
            fovList.Add(hmdDesc.DefaultEyeFov[0]);
            fovList.Add(hmdDesc.DefaultEyeFov[1]);
            hmd.ConfigureRendering(config, DistortionCap.DistortionCap_Chromatic | DistortionCap.DistortionCap_TimeWarp | DistortionCap.DistortionCap_Vignette, fovList, eyeList);
            hmd.AttachToWindow(form.Handle, null, null);

            // Use clock
            var clock = new Stopwatch();
            clock.Start();

            // Main loop
            uint frameIndex = 0;
            RenderLoop.Run(form, () =>
            {
                frameIndex++;
                var time = clock.ElapsedMilliseconds / 1000.0f;

                FrameTiming timing = hmd.BeginFrame(frameIndex);

                // Prepare All the stages
                context.InputAssembler.InputLayout = layout;
                context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
                context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vertices, Utilities.SizeOf<Vector4>() + Utilities.SizeOf<Vector2>(), 0));
                context.VertexShader.SetConstantBuffer(0, contantBuffer);
                context.VertexShader.Set(vertexShader);
                context.Rasterizer.SetViewport(new Viewport(0, 0, form.ClientSize.Width, form.ClientSize.Height, 0.0f, 1.0f));
                context.PixelShader.Set(pixelShader);
                context.PixelShader.SetSampler(0, sampler);
                context.PixelShader.SetShaderResource(0, textureView);
                context.OutputMerger.SetTargets(depthView, renderView);

                RasterizerStateDescription rs;
                rs = RasterizerStateDescription.Default();
                rs.CullMode = CullMode.None;
                context.Rasterizer.State = new RasterizerState(device, rs);

                context.OutputMerger.SetRenderTargets(depthView, renderTargetView);
                context.ClearRenderTargetView(renderTargetView, Color.White);
                context.ClearDepthStencilView(depthView, DepthStencilClearFlags.Depth, 1.0f, 0);

                List<Posef> poseList = new List<Posef>();
                List<D3D11TextureData> textureList = new List<D3D11TextureData>();
                poseList.Add(hmd.GetEyePose(hmdDesc.EyeRenderOrder[0]));
                poseList.Add(hmd.GetEyePose(hmdDesc.EyeRenderOrder[1]));
                textureList.Add(Render(hmdDesc.EyeRenderOrder[0], poseList[0], time, context, contantBuffer, eyeList[(int)hmdDesc.EyeRenderOrder[0]]));
                textureList.Add(Render(hmdDesc.EyeRenderOrder[1], poseList[1], time, context, contantBuffer, eyeList[(int)hmdDesc.EyeRenderOrder[1]]));

                context.Rasterizer.SetViewport(new ViewportF(0, 0, renderTargetSize.w, renderTargetSize.h));
                context.OutputMerger.SetTargets(depthView, renderView);
                context.ClearDepthStencilView(depthView, DepthStencilClearFlags.Depth, 1.0f, 0);
                context.ClearRenderTargetView(renderView, Color.Black);

                // Present!
                hmd.EndFrame(poseList.ToArray(), textureList.ToArray());
            });

            hmd.Dispose();
            OculusSharp.CAPI.Hmd.Shutdown();

            // Release all resources
            vertexShaderByteCode.Dispose();
            vertexShader.Dispose();
            pixelShaderByteCode.Dispose();
            pixelShader.Dispose();
            vertices.Dispose();
            layout.Dispose();
            renderView.Dispose();
            backBuffer.Dispose();
            context.ClearState();
            context.Flush();
            device.Dispose();
            context.Dispose();
            swapChain.Dispose();
            factory.Dispose();
        }

        static void form_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            HSWDisplayState hswDisplayState = new HSWDisplayState();
            hmd.GetHSWDisplayState(ref hswDisplayState);
            if (hswDisplayState.Displayed)
                hmd.DismissHSWDisplay();
        }

        public static D3D11TextureData Render(EyeType eyeType, Posef eyePose, float time, SharpDX.Direct3D11.DeviceContext context, SharpDX.Direct3D11.Buffer contantBuffer, EyeRenderDesc eyeRender)
        {
            SharpDX.Quaternion orientation = new Quaternion(eyePose.Orientation.x, eyePose.Orientation.y, eyePose.Orientation.z, eyePose.Orientation.w);
            orientation.Invert();

            Matrix4f _proj = Hmd.Matrix4f_Projection(eyeRender.Fov, 0.01f, 10000.0f, true); 
            SharpDX.Matrix proj = new Matrix(_proj.M00,_proj.M01,_proj.M02,_proj.M03,
                                            _proj.M10,_proj.M11,_proj.M12,_proj.M13,
                                            _proj.M20,_proj.M21,_proj.M22,_proj.M23,
                                            _proj.M30,_proj.M31,_proj.M32,_proj.M33);
            proj.Transpose();

            SharpDX.Matrix view = SharpDX.Matrix.Translation(eyeRender.ViewAdjust.x, eyeRender.ViewAdjust.y, eyeRender.ViewAdjust.z)
                                * SharpDX.Matrix.Translation(eyePose.Position.x, eyePose.Position.y, eyePose.Position.z) 
                                * SharpDX.Matrix.Translation(0, 0, -5) 
                                * SharpDX.Matrix.RotationQuaternion(orientation);
            var viewProj = Matrix.Multiply(view, proj);

            var worldViewProj = Matrix.RotationX(time) * Matrix.RotationY(time * 2) * Matrix.RotationZ(time * .7f) * viewProj;
            worldViewProj.Transpose();
            context.UpdateSubresource(ref worldViewProj, contantBuffer);


            D3D11TextureData eyeTxr = new D3D11TextureData();
            eyeTxr.Header.API = RenderAPIType.RenderAPI_D3D11;
            eyeTxr.Header.TextureSize = renderTargetSize;
            eyeTxr.pTexture = renderTargetTexture.NativePointer;
            eyeTxr.pSRView = renderTargetSRV.NativePointer;

            ViewportF viewport;
            if (eyeType == EyeType.Eye_Left)
                viewport = new ViewportF(0, 0, renderTargetSize.w / 2, renderTargetSize.h);
            else
                viewport = new ViewportF((renderTargetSize.w + 1) / 2, 0, renderTargetSize.w / 2, renderTargetSize.h);

            context.Rasterizer.SetViewport(viewport);
            eyeTxr.Header.RenderViewport.Pos.x = (int)viewport.X;
            eyeTxr.Header.RenderViewport.Pos.y = (int)viewport.Y;
            eyeTxr.Header.RenderViewport.Size.w = (int)viewport.Width;
            eyeTxr.Header.RenderViewport.Size.h = (int)viewport.Height;

            // Draw the cube
            context.Draw(36, 0);

            return eyeTxr;
        }
    }
}