#pragma once

#include "OVR.h"
#include "OVR_DeviceHandle.h"
#include <msclr\auto_gcroot.h>
#include "ScopedPtr.h"

namespace OculusSharp {

	ref class Profile;
	ref class ProfileManager;
	ref class SensorDevice;
	ref class DeviceCommon;
	ref class DeviceManager;
	ref class MessageHandler;
	ref class Message;

	ref class DeviceInfo
	{
	protected:
		clr_scoped_ptr<OVR::DeviceInfo> impl;
	public:
		DeviceInfo() { impl = new OVR::DeviceInfo(); }
		DeviceInfo(OVR::DeviceInfo& info) { impl = new OVR::DeviceInfo(info); }
		property DeviceType InfoClassType { DeviceType get(); }
		property DeviceType Type { DeviceType get(); }
		property unsigned Version { unsigned get(); }
		property System::String^ Manufacturer { System::String^ get(); }
		property System::String^ ProductName { System::String^ get(); }
	};


	class MessageHandlerImpl : public OVR::MessageHandler
	{
	public:
		MessageHandlerImpl(OculusSharp::MessageHandler^ mh) { handler = mh; }
		msclr::auto_gcroot<OculusSharp::MessageHandler^> handler;
		virtual void OnMessage(OVR::Message& msg);
		virtual bool SupportsMessageType(OVR::MessageType);
	};

	delegate bool SupportsMessageTypeDelegate(MessageType type);
	delegate bool OnMessageDelegate(Message^ msg);

	ref class MessageHandler
	{
		MessageHandlerImpl* impl;

	public:
		MessageHandler() { impl = new MessageHandlerImpl(this); }
		~MessageHandler() { delete this; }
		!MessageHandler() { delete impl; }
		MessageHandlerImpl* Native() { return impl; }

		void RaiseOnMessage(Message^ msg) { OnMessage(msg); }
		bool RaiseSupportsMessageType(MessageType type) { return SupportsMessageType(type); }
		event SupportsMessageTypeDelegate^ SupportsMessageType;
		event OnMessageDelegate^ OnMessage;

		bool IsHandlerInstalled() { return impl->IsHandlerInstalled(); }
		void RemoveHandlerFromDevices() { return impl->RemoveHandlerFromDevices(); }
		Lock^ GetHandlerLock() { return gcnew Lock(impl->GetHandlerLock()); }
	};


	ref class DeviceManager;
	ref class DeviceBase
	{
	protected:
		OVR::DeviceBase* impl;
	public:
		DeviceBase::DeviceBase(OVR::DeviceBase* impl) { this->impl = impl; impl->AddRef(); }
		DeviceBase::~DeviceBase() { delete this; }
		DeviceBase::!DeviceBase() { if (impl != NULL) impl->Release(); impl = NULL; }
		OVR::DeviceBase* DeviceBase::Native() { return impl; }

		DeviceBase^ GetParent();
		DeviceManager^ GetManager();
		void AddMessageHandler(MessageHandler^ handler);
		DeviceType GetType();
		DeviceInfo^ GetDeviceInfo();
		bool IsConnected();
		Lock^ GetHandlerLock();
	};
	
	ref class DeviceEnumerator : public DeviceHandle
	{
	public:
		DeviceEnumerator(OVR::DeviceEnumerator<OVR::DeviceBase> impl) : DeviceHandle(impl) { }
		bool Next() { return ((OVR::DeviceEnumerator<OVR::DeviceBase>*)impl.get())->Next(); }
	};

	ref class DeviceManager : DeviceBase
	{
	public:
		DeviceManager(OVR::DeviceManager* impl) : DeviceBase(impl) {  }
		ProfileManager^ GetProfileManager();
		DeviceEnumerator^ EnumerateDevicesEx(OculusSharp::DeviceType EnumType, bool AvailableOnly);
		static DeviceManager^ Create();
	};

	ref class ShutterInfo
	{
	protected:
		clr_scoped_ptr<OVR::HMDInfo::ShutterInfo> impl;
	public:
		ShutterInfo(OVR::HMDInfo::ShutterInfo& info) { impl = new OVR::HMDInfo::ShutterInfo(info); }
		OVR::HMDInfo::ShutterInfo* Native() { return impl.get(); }
		property float VsyncToNextVsync { float get() { return impl.get()->VsyncToNextVsync; } }
		property float VsyncToFirstScanline { float get() { return impl.get()->VsyncToFirstScanline; } }
		property float FirstScanlineToLastScanline { float get() { return impl.get()->FirstScanlineToLastScanline; } }
		property float PixelSettleTime { float get() { return impl.get()->PixelSettleTime; } }
		property float PixelPersistence { float get() { return impl.get()->PixelPersistence; } }
	};

	ref class HMDInfo : DeviceInfo
	{
	public:
		HMDInfo() : DeviceInfo() {  }
		HMDInfo(OVR::HMDInfo& info) : DeviceInfo(info) { }
		OVR::HMDInfo* Native() { return ((OVR::HMDInfo*)impl.get()); }

		property HmdTypeEnum HmdType { HmdTypeEnum get(); }
		property System::String^ DisplayDeviceName { System::String^ get(); }
		property System::Drawing::Size^ ResolutionInPixels { System::Drawing::Size^ get(); }
		property System::Drawing::SizeF^ ScreenSizeInMeters { System::Drawing::SizeF^ get(); }
		property float ScreenGapSizeInMeters { float get(); }
		property float CenterFromTopInMeters { float get(); }
		property float LensSeparationInMeters { float get(); }
		property ShutterInfo^ Shutter { ShutterInfo^ get(); }
		property int DesktopX { int get(); }
		property int DesktopY { int get(); }
		property int DisplayId { int get(); }
		bool IsSameDisplay(HMDInfo^ other);
	};

	ref class HMDDevice : DeviceBase
	{
	public:
		HMDDevice(OVR::HMDDevice* impl) : DeviceBase(impl){ }

		SensorDevice^ GetSensor();
		Profile^    GetProfile();
		System::String^ GetProfileName();
		bool        SetProfileName(System::String^ name);
		HMDDevice^  Disconnect(SensorDevice^ sensor);
		bool        IsDisconnected();
	};

	ref class SensorRange
	{
	protected:
		clr_scoped_ptr<OVR::SensorRange> impl;
	public:
		SensorRange() { impl = new OVR::SensorRange(); }
		SensorRange(OVR::SensorRange& info) { impl = new OVR::SensorRange(info); }
		SensorRange(SensorRange% info) { impl = new OVR::SensorRange(*info.impl); }
		OVR::SensorRange* Native() { return (OVR::SensorRange*)impl.get(); }
		property float MaxAcceleration { float get() { return impl.get()->MaxAcceleration; } void set(float value) { impl.get()->MaxAcceleration = value; } }
		property float MaxRotationRate { float get() { return impl.get()->MaxRotationRate; } void set(float value) { impl.get()->MaxRotationRate = value; } }
		property float MaxMagneticField { float get() { return impl.get()->MaxMagneticField; } void set(float value) { impl.get()->MaxMagneticField = value; } }
	};

	ref class SensorInfo : DeviceInfo
	{
	public:
		SensorInfo() : DeviceInfo() {  }
		SensorInfo(OVR::SensorInfo& info) : DeviceInfo(info) { }
		OVR::SensorInfo* Native() { return (OVR::SensorInfo*)impl.get(); }
		property UInt16 VendorId { UInt16 get(); }
		property UInt16 ProductId { UInt16 get(); }
		property SensorRange^ MaxRanges { SensorRange^ get(); }
		property System::String^ SerialNumber { System::String^ get(); }
	};

	ref class SerialReport
	{
	protected:
		clr_scoped_ptr<OVR::SerialReport> impl;
	public:
		SerialReport() { impl = new OVR::SerialReport(); }
		SerialReport(OVR::SerialReport& info) { impl = new OVR::SerialReport(info); }
		OVR::SerialReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property cli::array<byte>^ SerialNumberValue 
		{ 
			cli::array<byte>^ get() 
			{ 
				cli::array<byte>^ a = gcnew cli::array<byte>(impl.get()->SERIAL_NUMBER_SIZE); 
				for (int idx = 0; idx < impl.get()->SERIAL_NUMBER_SIZE; idx++) 
					a[idx] = impl.get()->SerialNumberValue[idx]; 
				return a; 
			} 
		}
	};

	ref class TrackingReport
	{
	protected:
		clr_scoped_ptr<OVR::TrackingReport> impl;
	public:
		TrackingReport() { impl = new OVR::TrackingReport(); }
		TrackingReport(OVR::TrackingReport& info) { impl = new OVR::TrackingReport(info); }
		OVR::TrackingReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property UInt16 ExposureLength { UInt16 get() { return impl.get()->ExposureLength; } }
		property UInt16 FrameInterval { UInt16 get() { return impl.get()->FrameInterval; } }
		property UInt16 VsyncOffset { UInt16 get() { return impl.get()->VsyncOffset; } }
		property byte Pattern { byte get() { return impl.get()->Pattern; } }
		property byte DutyCycle { byte get() { return impl.get()->DutyCycle; } }
		property bool Enable { bool get() { return impl.get()->Enable; } }
		property bool Autoincrement { bool get() { return impl.get()->Autoincrement; } }
		property bool UseCarrier { bool get() { return impl.get()->UseCarrier; } }
		property bool SyncInput { bool get() { return impl.get()->SyncInput; } }
		property bool VsyncLock { bool get() { return impl.get()->VsyncLock; } }
		property bool CustomPattern { bool get() { return impl.get()->CustomPattern; } }
	};

	ref class DisplayReport
	{
	protected:
		clr_scoped_ptr<OVR::DisplayReport> impl;
	public:
		DisplayReport() { impl = new OVR::DisplayReport(); }
		DisplayReport(OVR::DisplayReport& info) { impl = new OVR::DisplayReport(info); }
		OVR::DisplayReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property UInt16 Persistence { UInt16 get() { return impl.get()->Persistence; } }
		property UInt16 LightingOffset { UInt16 get() { return impl.get()->LightingOffset; }  }
		property UInt16 PixelSettle { UInt16 get() { return impl.get()->PixelSettle; }  }
		property UInt16 VsyncOffset { UInt16 get() { return impl.get()->TotalRows; } }
		property UInt16 ShutterType { UInt16 get() { return impl.get()->ShutterType; }  }
		property UInt16 CurrentLimit { UInt16 get() { return impl.get()->CurrentLimit; } }
		property byte Brightness { byte get() { return impl.get()->Brightness; } }
		property bool UseRolling { bool get() { return impl.get()->UseRolling; } }
		property bool ReverseRolling { bool get() { return impl.get()->ReverseRolling; } }
		property bool HighBrightness { bool get() { return impl.get()->HighBrightness; } }
		property bool SelfRefresh { bool get() { return impl.get()->SelfRefresh; } }
		property bool ReadPixel { bool get() { return impl.get()->ReadPixel; } }
		property bool DirectPentile { bool get() { return impl.get()->DirectPentile; } }
	};


	ref class MagCalibrationReport
	{
	protected:
		clr_scoped_ptr<OVR::MagCalibrationReport> impl;
	public:
		MagCalibrationReport() { impl = new OVR::MagCalibrationReport(); }
		MagCalibrationReport(OVR::MagCalibrationReport& info) { impl = new OVR::MagCalibrationReport(info); }
		OVR::MagCalibrationReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; }  }
		property byte Version { byte get() { return impl.get()->Version; } }
		property SharpDX::Matrix^ Calibration 
		{ 
			SharpDX::Matrix^ get() 
			{ 
				return gcnew SharpDX::Matrix(impl.get()->Calibration.M[0][0], impl.get()->Calibration.M[0][1], impl.get()->Calibration.M[0][2], impl.get()->Calibration.M[0][3],
					impl.get()->Calibration.M[1][0], impl.get()->Calibration.M[1][1], impl.get()->Calibration.M[1][2], impl.get()->Calibration.M[1][3],
					impl.get()->Calibration.M[2][0], impl.get()->Calibration.M[2][1], impl.get()->Calibration.M[2][2], impl.get()->Calibration.M[2][3],
					impl.get()->Calibration.M[3][0], impl.get()->Calibration.M[3][1], impl.get()->Calibration.M[3][2], impl.get()->Calibration.M[3][3]);
			} 
		}
	};

	ref class PositionCalibrationReport
	{
	protected:
		clr_scoped_ptr<OVR::PositionCalibrationReport> impl;
	public:
		PositionCalibrationReport() { impl = new OVR::PositionCalibrationReport(); }
		PositionCalibrationReport(OVR::PositionCalibrationReport& info) { impl = new OVR::PositionCalibrationReport(info); }
		OVR::PositionCalibrationReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property UInt16 PositionIndex { UInt16 get() { return impl.get()->PositionIndex; } }
		property UInt16 NumPositions { UInt16 get() { return impl.get()->NumPositions; } }
		property UInt16 PositionType { UInt16 get() { return impl.get()->PositionType; } }
		property byte Version { byte get() { return impl.get()->Version; } }
		property double Angle { double get() { return impl.get()->Angle; } }
		property SharpDX::Vector3^ Position { SharpDX::Vector3^ get() { return gcnew SharpDX::Vector3(impl.get()->Position.x, impl.get()->Position.y, impl.get()->Position.z); } }
		property SharpDX::Vector3^ Normal { SharpDX::Vector3^ get() { return gcnew SharpDX::Vector3(impl.get()->Normal.x, impl.get()->Normal.y, impl.get()->Normal.z); } }
	};

	ref class CustomPatternReport
	{
	protected:
		clr_scoped_ptr<OVR::CustomPatternReport> impl;
	public:
		CustomPatternReport() { impl = new OVR::CustomPatternReport(); }
		CustomPatternReport(OVR::CustomPatternReport& info) { impl = new OVR::CustomPatternReport(info); }
		OVR::CustomPatternReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; }}
		property UInt16 LEDIndex { UInt16 get() { return impl.get()->LEDIndex; }}
		property UInt16 NumLEDs { UInt16 get() { return impl.get()->NumLEDs; }}
		property UInt32 Sequence { UInt32 get() { return impl.get()->Sequence; }}
		property byte SequenceLength { byte get() { return impl.get()->SequenceLength; }}
	};

	ref class KeepAliveMuxReport
	{
	protected:
		clr_scoped_ptr<OVR::KeepAliveMuxReport> impl;
	public:
		KeepAliveMuxReport() { impl = new OVR::KeepAliveMuxReport(); }
		KeepAliveMuxReport(OVR::KeepAliveMuxReport& info) { impl = new OVR::KeepAliveMuxReport(info); }
		OVR::KeepAliveMuxReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property UInt16 Interval { UInt16 get() { return impl.get()->Interval; } }
		property byte INReport { byte get() { return impl.get()->INReport; } }
	};

	ref class ManufacturingReport
	{
	protected:
		clr_scoped_ptr<OVR::ManufacturingReport> impl;
	public:
		ManufacturingReport() { impl = new OVR::ManufacturingReport(); }
		ManufacturingReport(OVR::ManufacturingReport& info) { impl = new OVR::ManufacturingReport(info); }
		OVR::ManufacturingReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property UInt16 StageLocation { UInt16 get() { return impl.get()->StageLocation; } }
		property UInt32 StageTime { UInt32 get() { return impl.get()->StageTime; } }
		property UInt32 Result { UInt32 get() { return impl.get()->Result; } }
		property byte NumStages { byte get() { return impl.get()->NumStages; } }
		property byte Stage { byte get() { return impl.get()->Stage; } }
		property byte StageVersion { byte get() { return impl.get()->StageVersion; } }
	};

	ref class UUIDReport
	{
	protected:
		clr_scoped_ptr<OVR::UUIDReport> impl;
	public:
		UUIDReport() { impl = new OVR::UUIDReport(); }
		UUIDReport(OVR::UUIDReport& info) { impl = new OVR::UUIDReport(info); }
		OVR::UUIDReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property cli::array<byte>^ UUID 
		{ 
			cli::array<byte>^ get() 
			{ 
				cli::array<byte>^ a = gcnew cli::array<byte>(impl.get()->UUID_SIZE); 
				for (int idx = 0; idx < impl.get()->UUID_SIZE; idx++) 
					a[idx] = impl.get()->UUIDValue[idx]; 
				return a; 
			} 
		}
	};

	ref class LensDistortionReport
	{
	protected:
		clr_scoped_ptr<OVR::LensDistortionReport> impl;
	public:
		LensDistortionReport() { impl = new OVR::LensDistortionReport(); }
		LensDistortionReport(OVR::LensDistortionReport& info) { impl = new OVR::LensDistortionReport(info); }
		OVR::LensDistortionReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property UInt16 LensType { UInt16 get() { return impl.get()->LensType; } }
		property UInt16 Version { UInt16 get() { return impl.get()->Version; } }
		property UInt16 EyeRelief { UInt16 get() { return impl.get()->EyeRelief; }  }
		property UInt16 KCoefficient0 { UInt16 get() { return impl.get()->KCoefficients[0]; } }
		property UInt16 KCoefficient1 { UInt16 get() { return impl.get()->KCoefficients[1]; } }
		property UInt16 KCoefficient2 { UInt16 get() { return impl.get()->KCoefficients[2]; } }
		property UInt16 KCoefficient3 { UInt16 get() { return impl.get()->KCoefficients[3]; } }
		property UInt16 KCoefficient4 { UInt16 get() { return impl.get()->KCoefficients[4]; } }
		property UInt16 KCoefficient5 { UInt16 get() { return impl.get()->KCoefficients[5]; } }
		property UInt16 KCoefficient6 { UInt16 get() { return impl.get()->KCoefficients[6]; } }
		property UInt16 KCoefficient7 { UInt16 get() { return impl.get()->KCoefficients[7]; } }
		property UInt16 KCoefficient8 { UInt16 get() { return impl.get()->KCoefficients[8]; } }
		property UInt16 KCoefficient9 { UInt16 get() { return impl.get()->KCoefficients[9]; } }
		property UInt16 KCoefficient10 { UInt16 get() { return impl.get()->KCoefficients[10]; } }
		property UInt16 MaxR { UInt16 get() { return impl.get()->MaxR; } }
		property UInt16 MetersPerTanAngleAtCenter { UInt16 get() { return impl.get()->MetersPerTanAngleAtCenter; } }
		property UInt16 ChromaticAberration0 { UInt16 get() { return impl.get()->ChromaticAberration[0]; } }
		property UInt16 ChromaticAberration1 { UInt16 get() { return impl.get()->ChromaticAberration[1]; } }
		property UInt16 ChromaticAberration2 { UInt16 get() { return impl.get()->ChromaticAberration[2]; } }
		property UInt16 ChromaticAberration3 { UInt16 get() { return impl.get()->ChromaticAberration[3]; } }
		property byte NumDistortions { byte get() { return impl.get()->NumDistortions; } }
		property byte DistortionIndex { byte get() { return impl.get()->DistortionIndex; } }
		property byte Bitmask { byte get() { return impl.get()->Bitmask; } }
	};

	ref class TemperatureReport
	{
	protected:
		clr_scoped_ptr<OVR::TemperatureReport> impl;
	public:
		TemperatureReport() { impl = new OVR::TemperatureReport(); }
		TemperatureReport(OVR::TemperatureReport& info) { impl = new OVR::TemperatureReport(info); }
		OVR::TemperatureReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; }  }
		property UInt32 Time { UInt32 get() { return impl.get()->Time; } }
		property SharpDX::Vector3^ Offset { SharpDX::Vector3^ get() { return gcnew SharpDX::Vector3(impl.get()->Offset.x, impl.get()->Offset.y, impl.get()->Offset.z); } }
		property double TargetTemperature { double get() { return impl.get()->TargetTemperature; }  }
		property double ActualTemperature { double get() { return impl.get()->ActualTemperature; } }
		property byte Version { byte get() { return impl.get()->Version; } }
		property byte NumBins { byte get() { return impl.get()->NumBins; } }
		property byte Bin { byte get() { return impl.get()->Bin; } }
		property byte NumSamples { byte get() { return impl.get()->NumSamples; } }
		property byte Sample { byte get() { return impl.get()->Sample; } }
	};

	ref class GyroOffsetReport
	{
	protected:
		clr_scoped_ptr<OVR::GyroOffsetReport> impl;
	public:
		GyroOffsetReport() { impl = new OVR::GyroOffsetReport(); }
		GyroOffsetReport(OVR::GyroOffsetReport& info) { impl = new OVR::GyroOffsetReport(info); }
		OVR::GyroOffsetReport* Native() { return impl.get(); }
		property UInt16 CommandId { UInt16 get() { return impl.get()->CommandId; } }
		property SharpDX::Vector3^ Offset { SharpDX::Vector3^ get() { return gcnew SharpDX::Vector3(impl.get()->Offset.x, impl.get()->Offset.y, impl.get()->Offset.z); } }
		property double Temperature { double get() { return impl.get()->Temperature; } }
		property byte Version { byte get() { return impl.get()->Version; } }
	};

	ref class SensorDevice : DeviceBase
	{
	public:
		SensorDevice(OVR::SensorDevice* impl) : DeviceBase(impl){ }
		enum class CoordinateFrame
		{
			Coord_Sensor = 0,
			Coord_HMD = 1
		};

		byte GetDeviceInterfaceVersion();
		void SetCoordinateFrame(CoordinateFrame coordframe);
		CoordinateFrame GetCoordinateFrame();
		void SetReportRate(unsigned rateHz);
		unsigned GetReportRate();
		bool SetRange(SensorRange^ range, bool waitFlag);
		void GetRange(SensorRange^ range);
		void GetFactoryCalibration(SharpDX::Vector3^% AccelOffset, SharpDX::Vector3^% GyroOffset, SharpDX::Matrix^% AccelMatrix, SharpDX::Matrix^% GyroMatrix, float% Temperature);
		void SetOnboardCalibrationEnabled(bool enabled);
		bool IsMagCalibrated();
		bool SetSerialReport(SerialReport^ r);
		SerialReport^ GetSerialReport();
		bool SetTrackingReport(TrackingReport^ r);
		TrackingReport^ GetTrackingReport();
		bool SetDisplayReport(DisplayReport^ r);
		DisplayReport^ GetDisplayReport();
		bool SetMagCalibrationReport(MagCalibrationReport^ r);
		MagCalibrationReport^ GetMagCalibrationReport();
		bool SetPositionCalibrationReport(PositionCalibrationReport^);
		cli::array<PositionCalibrationReport^>^ GetAllPositionCalibrationReports();
		bool SetCustomPatternReport(CustomPatternReport^ r);
		CustomPatternReport^ GetCustomPatternReport();
		bool SetKeepAliveMuxReport(KeepAliveMuxReport^ r);
		KeepAliveMuxReport^ GetKeepAliveMuxReport();
		bool SetManufacturingReport(ManufacturingReport^ r);
		ManufacturingReport^ GetManufacturingReport();
		bool SetUUIDReport(UUIDReport^ r);
		UUIDReport^ GetUUIDReport();
		bool SetTemperatureReport(TemperatureReport^);
		cli::array<cli::array<TemperatureReport^>^>^ GetAllTemperatureReports();
		GyroOffsetReport^ GetGyroOffsetReport();
		bool SetLensDistortionReport(LensDistortionReport^ r);
		LensDistortionReport^ GetLensDistortionReport();
	};

	ref class LatencyTestConfiguration
	{
	protected:
		clr_scoped_ptr<OVR::LatencyTestConfiguration> impl;
	public:
		LatencyTestConfiguration() { impl = new OVR::LatencyTestConfiguration(OVR::Color()); }
		LatencyTestConfiguration(OVR::LatencyTestConfiguration& info) { impl = new OVR::LatencyTestConfiguration(info); }
		OVR::LatencyTestConfiguration* Native() { return impl.get(); }
		property bool SendSamples { bool get() { return impl.get()->SendSamples; } void set(bool value) { impl.get()->SendSamples = value; } }
		property SharpDX::ColorBGRA Threshold { SharpDX::ColorBGRA get() { return SharpDX::ColorBGRA(impl.get()->Threshold.R, impl.get()->Threshold.G, impl.get()->Threshold.B, impl.get()->Threshold.A); } void set(SharpDX::ColorBGRA value) { impl.get()->Threshold = OVR::Color(value.R, value.G, value.B, value.A); } }
	};

	ref class LatencyTestDisplay
	{
	protected:
		clr_scoped_ptr<OVR::LatencyTestDisplay> impl;
	public:
		LatencyTestDisplay(byte mode, UInt32 value) { impl = new OVR::LatencyTestDisplay(mode, value); }
		LatencyTestDisplay(OVR::LatencyTestDisplay& info) { impl = new OVR::LatencyTestDisplay(info); }
		OVR::LatencyTestDisplay* Native() { return impl.get(); }
		property byte Mode { byte get() { return impl.get()->Mode; } void set(byte value) { impl.get()->Mode = value; } }
		property UInt32 Value { UInt32 get() { return impl.get()->Value; } void set(UInt32 value) { impl.get()->Value = value; } }
	};

	ref class LatencyTestDevice : DeviceBase
	{
	public:
		LatencyTestDevice(OVR::LatencyTestDevice* impl) : DeviceBase(impl){ }

		bool SetConfiguration(LatencyTestConfiguration^ configuration, bool waitFlag) { return ((OVR::LatencyTestDevice*)impl)->SetConfiguration(*configuration->Native(), waitFlag); }
		LatencyTestConfiguration^ GetConfiguration() { OVR::Color c; OVR::LatencyTestConfiguration r(c); if (((OVR::LatencyTestDevice*)impl)->GetConfiguration(&r)) return gcnew LatencyTestConfiguration(r); else return nullptr; }
		bool SetCalibrate(SharpDX::ColorBGRA c, bool waitFlag) { return ((OVR::LatencyTestDevice*)impl)->SetCalibrate(OVR::Color(c.R, c.G, c.B, c.A), waitFlag); }
		bool SetStartTest(SharpDX::ColorBGRA c, bool waitFlag) { return ((OVR::LatencyTestDevice*)impl)->SetStartTest(OVR::Color(c.R, c.G, c.B, c.A), waitFlag); }
		bool SetDisplay(LatencyTestDisplay^ display, bool waitFlag) { return ((OVR::LatencyTestDevice*)impl)->SetDisplay(*display->Native(), waitFlag); }
	};


}