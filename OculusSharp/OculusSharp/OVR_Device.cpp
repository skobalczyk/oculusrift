// This is the main DLL file.

#include "stdafx.h"
#include <msclr\marshal_cppstd.h>

namespace OculusSharp {

	// ---------------------------------------------------------
	// --- DeviceInfo
	DeviceType DeviceInfo::InfoClassType::get() { return (DeviceType)(impl.get()->InfoClassType); }
	DeviceType DeviceInfo::Type::get() { return (DeviceType)impl.get()->Type; }
	unsigned DeviceInfo::Version::get() { return impl.get()->Version; }
	System::String^ DeviceInfo::Manufacturer::get() { return gcnew System::String(impl.get()->Manufacturer); }
	System::String^ DeviceInfo::ProductName::get() { return gcnew System::String(impl.get()->ProductName); }

	// ---------------------------------------------------------
	// --- MessageHandlerImpl
	void MessageHandlerImpl::OnMessage(OVR::Message& msg) { handler->RaiseOnMessage(gcnew Message(msg)); }
	bool MessageHandlerImpl::SupportsMessageType(OVR::MessageType) { return true; }

	// ---------------------------------------------------------
	// --- DeviceBase
	DeviceBase^ DeviceBase::GetParent() { return gcnew DeviceBase(impl->GetParent()); }
	DeviceManager^ DeviceBase::GetManager() { return gcnew DeviceManager(impl->GetManager()); }
	void DeviceBase::AddMessageHandler(MessageHandler^ handler) { impl->AddMessageHandler(handler->Native()); }
	DeviceType DeviceBase::GetType() { return (DeviceType)impl->GetType(); }
	DeviceInfo^ DeviceBase::GetDeviceInfo()
	{
		OVR::DeviceInfo info;
		if (!impl->GetDeviceInfo(&info))
			return nullptr;
		else
			return gcnew DeviceInfo(info);
	}
	bool DeviceBase::IsConnected() { return impl->IsConnected(); }
	Lock^ DeviceBase::GetHandlerLock() { return gcnew Lock(impl->GetHandlerLock()); }

	// ---------------------------------------------------------
	// --- DeviceManager
	ProfileManager^ DeviceManager::GetProfileManager() { return gcnew ProfileManager(((OVR::DeviceManager*)impl)->GetProfileManager()); }
	DeviceEnumerator^ DeviceManager::EnumerateDevicesEx(OculusSharp::DeviceType EnumType, bool AvailableOnly)
	{ 
		return gcnew DeviceEnumerator(((OVR::DeviceManager*)impl)->EnumerateDevicesEx(OVR::DeviceEnumerationArgs((OVR::DeviceType)EnumType, AvailableOnly))); 
	}
	DeviceManager^ DeviceManager::Create() { return gcnew DeviceManager(OVR::DeviceManager::Create()); }


	// ---------------------------------------------------------
	// --- HMDInfo
	HmdTypeEnum HMDInfo::HmdType::get() { return (HmdTypeEnum)((OVR::HMDInfo*)impl.get())->HmdType; }
	System::String^ HMDInfo::DisplayDeviceName::get() { return gcnew System::String(((OVR::HMDInfo*)impl.get())->DisplayDeviceName); }
	System::Drawing::Size^ HMDInfo::ResolutionInPixels::get() { return gcnew System::Drawing::Size(((OVR::HMDInfo*)impl.get())->ResolutionInPixels.w, ((OVR::HMDInfo*)impl.get())->ResolutionInPixels.h); }
	System::Drawing::SizeF^ HMDInfo::ScreenSizeInMeters::get() { return gcnew System::Drawing::SizeF(((OVR::HMDInfo*)impl.get())->ScreenSizeInMeters.w, ((OVR::HMDInfo*)impl.get())->ScreenSizeInMeters.h); }
	float HMDInfo::ScreenGapSizeInMeters::get() { return ((OVR::HMDInfo*)impl.get())->ScreenGapSizeInMeters; }
	float HMDInfo::CenterFromTopInMeters::get() { return ((OVR::HMDInfo*)impl.get())->CenterFromTopInMeters; }
	float HMDInfo::LensSeparationInMeters::get() { return ((OVR::HMDInfo*)impl.get())->LensSeparationInMeters; }
	ShutterInfo^ HMDInfo::Shutter::get() { return gcnew ShutterInfo(((OVR::HMDInfo*)impl.get())->Shutter); }
	int HMDInfo::DesktopX::get() { return ((OVR::HMDInfo*)impl.get())->DesktopX; }
	int HMDInfo::DesktopY::get() { return ((OVR::HMDInfo*)impl.get())->DesktopY; }
	int HMDInfo::DisplayId::get() { return ((OVR::HMDInfo*)impl.get())->DisplayId; }
	bool HMDInfo::IsSameDisplay(HMDInfo^ other) { return ((OVR::HMDInfo*)impl.get())->IsSameDisplay(*other->Native()); }

	// ---------------------------------------------------------
	// --- SensorInfo
	UInt16 SensorInfo::VendorId::get() { return ((OVR::SensorInfo*)impl.get())->VendorId; }
	UInt16 SensorInfo::ProductId::get() { return ((OVR::SensorInfo*)impl.get())->ProductId; }
	SensorRange^ SensorInfo::MaxRanges::get() { return gcnew SensorRange(((OVR::SensorInfo*)impl.get())->MaxRanges); }
	System::String^ SensorInfo::SerialNumber::get() { return gcnew System::String(((OVR::SensorInfo*)impl.get())->ProductName); }

	// ---------------------------------------------------------
	// --- HMDDevice
	SensorDevice^ HMDDevice::GetSensor() { return gcnew SensorDevice(((OVR::HMDDevice*)impl)->GetSensor()); }
	Profile^ HMDDevice::GetProfile() { return gcnew Profile(((OVR::HMDDevice*)impl)->GetProfile()); }
	System::String^ HMDDevice::GetProfileName() { return gcnew System::String(((OVR::HMDDevice*)impl)->GetProfileName()); }
	bool HMDDevice::SetProfileName(System::String^ name) { std::string stdString = msclr::interop::marshal_as<std::string>(name); return ((OVR::HMDDevice*)impl)->SetProfileName(stdString.c_str()); }
	HMDDevice^  HMDDevice::Disconnect(SensorDevice^ sensor) { return gcnew HMDDevice(((OVR::HMDDevice*)impl)->Disconnect((OVR::SensorDevice*)sensor->Native())); }
	bool HMDDevice::IsDisconnected() { return ((OVR::HMDDevice*)impl)->IsDisconnected(); }

	// ---------------------------------------------------------
	// --- SensorDevice
	byte SensorDevice::GetDeviceInterfaceVersion() { return ((OVR::SensorDevice*)impl)->GetDeviceInterfaceVersion(); }
	void SensorDevice::SetCoordinateFrame(CoordinateFrame coordframe) { ((OVR::SensorDevice*)impl)->SetCoordinateFrame((OVR::SensorDevice::CoordinateFrame)coordframe); }
	SensorDevice::CoordinateFrame SensorDevice::GetCoordinateFrame() { return (SensorDevice::CoordinateFrame)((OVR::SensorDevice*)impl)->GetCoordinateFrame(); }
	void SensorDevice::SetReportRate(unsigned rateHz) { ((OVR::SensorDevice*)impl)->SetReportRate(rateHz); }
	unsigned SensorDevice::GetReportRate() { return ((OVR::SensorDevice*)impl)->GetReportRate(); }
	bool SensorDevice::SetRange(SensorRange^ range, bool waitFlag) { return ((OVR::SensorDevice*)impl)->SetRange(*range->Native()); }
	void SensorDevice::GetRange(SensorRange^ range) { OVR::SensorRange r; ((OVR::SensorDevice*)impl)->GetRange(&r); range = gcnew SensorRange(r); }
	void SensorDevice::GetFactoryCalibration(SharpDX::Vector3^% AccelOffset, SharpDX::Vector3^% GyroOffset, SharpDX::Matrix^% AccelMatrix, SharpDX::Matrix^% GyroMatrix, float% Temperature)
	{
		OVR::Vector3f accelOffset;
		OVR::Vector3f gyroOffset;
		OVR::Matrix4f accelMatrix;
		OVR::Matrix4f gyroMatrix;
		float temp;
		((OVR::SensorDevice*)impl)->GetFactoryCalibration(&accelOffset, &gyroOffset, &accelMatrix, &gyroMatrix, &temp);
		Temperature = temp;
		AccelOffset = gcnew SharpDX::Vector3(accelOffset.x, accelOffset.y, accelOffset.z);
		GyroOffset = gcnew SharpDX::Vector3(gyroOffset.x, gyroOffset.y, gyroOffset.z);
		AccelMatrix = gcnew SharpDX::Matrix(accelMatrix.M[0][0], accelMatrix.M[0][1], accelMatrix.M[0][2], accelMatrix.M[0][3],
			accelMatrix.M[1][0], accelMatrix.M[1][1], accelMatrix.M[1][2], accelMatrix.M[1][3],
			accelMatrix.M[2][0], accelMatrix.M[2][1], accelMatrix.M[2][2], accelMatrix.M[2][3],
			accelMatrix.M[3][0], accelMatrix.M[3][1], accelMatrix.M[3][2], accelMatrix.M[3][3]);
		GyroMatrix = gcnew SharpDX::Matrix(gyroMatrix.M[0][0], gyroMatrix.M[0][1], gyroMatrix.M[0][2], gyroMatrix.M[0][3],
			gyroMatrix.M[1][0], gyroMatrix.M[1][1], gyroMatrix.M[1][2], gyroMatrix.M[1][3],
			gyroMatrix.M[2][0], gyroMatrix.M[2][1], gyroMatrix.M[2][2], gyroMatrix.M[2][3],
			gyroMatrix.M[3][0], gyroMatrix.M[3][1], gyroMatrix.M[3][2], gyroMatrix.M[3][3]);
	}
	void SensorDevice::SetOnboardCalibrationEnabled(bool enabled) { ((OVR::SensorDevice*)impl)->SetOnboardCalibrationEnabled(enabled); }
	bool SensorDevice::IsMagCalibrated() { return ((OVR::SensorDevice*)impl)->IsMagCalibrated(); }
	bool SensorDevice::SetSerialReport(SerialReport^ r) { return ((OVR::SensorDevice*)impl)->SetSerialReport(*r->Native()); }
	SerialReport^ SensorDevice::GetSerialReport() { OVR::SerialReport r; if (((OVR::SensorDevice*)impl)->GetSerialReport(&r)) return gcnew SerialReport(r); else return nullptr; }
	bool SensorDevice::SetTrackingReport(TrackingReport^ r) { return ((OVR::SensorDevice*)impl)->SetTrackingReport(*r->Native()); }
	TrackingReport^ SensorDevice::GetTrackingReport() { OVR::TrackingReport r; if (((OVR::SensorDevice*)impl)->GetTrackingReport(&r)) return gcnew TrackingReport(r); else return nullptr; }
	bool SensorDevice::SetDisplayReport(DisplayReport^ r) { return ((OVR::SensorDevice*)impl)->SetDisplayReport(*r->Native()); }
	DisplayReport^ SensorDevice::GetDisplayReport() { OVR::DisplayReport r; if (((OVR::SensorDevice*)impl)->GetDisplayReport(&r)) return gcnew DisplayReport(r); else return nullptr; }
	bool SensorDevice::SetMagCalibrationReport(MagCalibrationReport^ r) { return ((OVR::SensorDevice*)impl)->SetMagCalibrationReport(*r->Native()); }
	MagCalibrationReport^ SensorDevice::GetMagCalibrationReport() { OVR::MagCalibrationReport r; if (((OVR::SensorDevice*)impl)->GetMagCalibrationReport(&r)) return gcnew MagCalibrationReport(r); else return nullptr; }
	bool SensorDevice::SetPositionCalibrationReport(PositionCalibrationReport^ r) { return ((OVR::SensorDevice*)impl)->SetPositionCalibrationReport(*r->Native()); }
	cli::array<PositionCalibrationReport^>^ SensorDevice::GetAllPositionCalibrationReports() { OVR::Array<OVR::PositionCalibrationReport> arr; ((OVR::SensorDevice*)impl)->GetAllPositionCalibrationReports(&arr); cli::array<PositionCalibrationReport^>^ res = gcnew cli::array<PositionCalibrationReport^>(arr.GetSize()); for (int idx = 0; idx < arr.GetSize(); idx++) res[idx] = gcnew PositionCalibrationReport(arr[idx]); return res; }
	bool SensorDevice::SetCustomPatternReport(CustomPatternReport^ r) { return ((OVR::SensorDevice*)impl)->SetCustomPatternReport(*r->Native()); }
	CustomPatternReport^ SensorDevice::GetCustomPatternReport() { OVR::CustomPatternReport r; if (((OVR::SensorDevice*)impl)->GetCustomPatternReport(&r)) return gcnew CustomPatternReport(r); else return nullptr; }
	bool SensorDevice::SetKeepAliveMuxReport(KeepAliveMuxReport^ r) { return ((OVR::SensorDevice*)impl)->SetKeepAliveMuxReport(*r->Native()); }
	KeepAliveMuxReport^ SensorDevice::GetKeepAliveMuxReport() { OVR::KeepAliveMuxReport r; if (((OVR::SensorDevice*)impl)->GetKeepAliveMuxReport(&r)) return gcnew KeepAliveMuxReport(r); else return nullptr; }
	bool SensorDevice::SetManufacturingReport(ManufacturingReport^ r) { return ((OVR::SensorDevice*)impl)->SetManufacturingReport(*r->Native()); }
	ManufacturingReport^ SensorDevice::GetManufacturingReport() { OVR::ManufacturingReport r; if (((OVR::SensorDevice*)impl)->GetManufacturingReport(&r)) return gcnew ManufacturingReport(r); else return nullptr; }
	bool SensorDevice::SetUUIDReport(UUIDReport^ r) { return ((OVR::SensorDevice*)impl)->SetUUIDReport(*r->Native()); }
	UUIDReport^ SensorDevice::GetUUIDReport() { OVR::UUIDReport r; if (((OVR::SensorDevice*)impl)->GetUUIDReport(&r)) return gcnew UUIDReport(r); else return nullptr; }
	bool SensorDevice::SetTemperatureReport(TemperatureReport^ r) { return ((OVR::SensorDevice*)impl)->SetTemperatureReport(*r->Native()); }
	cli::array<cli::array<TemperatureReport^>^>^ SensorDevice::GetAllTemperatureReports()
	{ 
		OVR::Array<OVR::Array<OVR::TemperatureReport>> arr; 
		((OVR::SensorDevice*)impl)->GetAllTemperatureReports(&arr); 
		cli::array<cli::array<TemperatureReport^>^>^ res = gcnew cli::array<cli::array<TemperatureReport^>^>(arr.GetSize());
		for (int idx = 0; idx < arr.GetSize(); idx++)
		{
			res[idx] = gcnew cli::array<TemperatureReport^>(arr[idx].GetSize());
			for (int idx2 = 0; idx2 < arr[idx].GetSize(); idx2++)
				res[idx][idx2] = gcnew TemperatureReport(arr[idx][idx2]); 
			return res;
		}
	}
	//		virtual bool		SensorDevice::SetTemperatureReport(const TemperatureReport&) { return false; }
	//		virtual bool        SensorDevice::GetAllTemperatureReports(Array<Array<TemperatureReport> >*) { return false; }
	GyroOffsetReport^ SensorDevice::GetGyroOffsetReport() { OVR::GyroOffsetReport r; if (((OVR::SensorDevice*)impl)->GetGyroOffsetReport(&r)) return gcnew GyroOffsetReport(r); else return nullptr; }
	bool SensorDevice::SetLensDistortionReport(LensDistortionReport^ r) { return ((OVR::SensorDevice*)impl)->SetLensDistortionReport(*r->Native()); }
	LensDistortionReport^ SensorDevice::GetLensDistortionReport() { OVR::LensDistortionReport r; if (((OVR::SensorDevice*)impl)->GetLensDistortionReport(&r)) return gcnew LensDistortionReport(r); else return nullptr; }

}
