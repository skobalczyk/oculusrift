#pragma once

#include "OVR.h"
#include "OVR_DeviceConstants.h"
#include "ScopedPtr.h"

namespace OculusSharp {

	ref class DeviceBase;
	ref class DeviceInfo;
	ref class DeviceHandle
	{
	protected:
		clr_scoped_ptr<OVR::DeviceHandle> impl;
	public:
		DeviceHandle(OVR::DeviceHandle& impl);
		DeviceHandle(DeviceHandle^ src);

		void operator = (DeviceHandle^ src);
		bool operator == (DeviceHandle^ other);
		bool operator != (DeviceHandle^ other);
		operator bool();

		DeviceBase^ GetDevice_AddRef();
		DeviceType GetType();
		DeviceInfo^ GetDeviceInfo();
		bool        IsAvailable();
		bool        IsCreated();
		bool        IsDevice(DeviceBase^ device);
		DeviceBase^ CreateDevice();
		void		Clear();
	};

}