#pragma once

#include "OVR.h"
#include <msclr\auto_gcroot.h>

namespace OculusSharp {

	ref class Lock
	{
	protected:
		OVR::Lock* impl;

	public:
		Lock(OVR::Lock*) { this->impl = impl; }

		inline void DoLock() { impl->DoLock(); }
		inline void Unlock() { impl->Unlock(); }
	};
}