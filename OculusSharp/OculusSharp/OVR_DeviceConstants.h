#pragma once

#include "OVR.h"

namespace OculusSharp {


	enum class DeviceType : byte
	{
		ENUM_ENTRY(Device_None)
		ENUM_ENTRY(Device_Manager)
		ENUM_ENTRY(Device_HMD)
		ENUM_ENTRY(Device_Sensor)
		ENUM_ENTRY(Device_LatencyTester)
		ENUM_ENTRY(Device_BootLoader)
		ENUM_ENTRY(Device_Camera)
		ENUM_ENTRY(Device_Display)
		ENUM_ENTRY(Device_All)
	};

	enum class DistortionEqnType
	{
		ENUM_ENTRY(Distortion_No_Override)
		ENUM_ENTRY(Distortion_Poly4)
		ENUM_ENTRY(Distortion_RecipPoly4)
		ENUM_ENTRY(Distortion_CatmullRom10)
		ENUM_ENTRY(Distortion_LAST)
	};


	enum class HmdTypeEnum
	{
		ENUM_ENTRY(HmdType_None)
		ENUM_ENTRY(HmdType_DKProto)
		ENUM_ENTRY(HmdType_DK1)
		ENUM_ENTRY(HmdType_DKHDProto)
		ENUM_ENTRY(HmdType_DKHD2Proto)
		ENUM_ENTRY(HmdType_DKHDProto566Mi)
		ENUM_ENTRY(HmdType_CrystalCoveProto)
		ENUM_ENTRY(HmdType_DK2)
		ENUM_ENTRY(HmdType_Unknown)
		ENUM_ENTRY(HmdType_LAST)
	};


	enum class HmdShutterTypeEnum
	{
		ENUM_ENTRY(HmdShutter_Global)
		ENUM_ENTRY(HmdShutter_RollingTopToBottom)
		ENUM_ENTRY(HmdShutter_RollingLeftToRight)
		ENUM_ENTRY(HmdShutter_RollingRightToLeft)
		ENUM_ENTRY(HmdShutter_LAST)
	};



	enum EyeCupType
	{
		ENUM_ENTRY(EyeCup_DK1A)
		ENUM_ENTRY(EyeCup_DK1B)
		ENUM_ENTRY(EyeCup_DK1C)
		ENUM_ENTRY(EyeCup_DK2A)
		ENUM_ENTRY(EyeCup_DKHD2A)
		ENUM_ENTRY(EyeCup_OrangeA)
		ENUM_ENTRY(EyeCup_RedA)
		ENUM_ENTRY(EyeCup_PinkA)
		ENUM_ENTRY(EyeCup_BlueA)
		ENUM_ENTRY(EyeCup_Delilah1A)
		ENUM_ENTRY(EyeCup_Delilah2A)
		ENUM_ENTRY(EyeCup_JamesA)
		ENUM_ENTRY(EyeCup_SunMandalaA)
		ENUM_ENTRY(EyeCup_LAST)
	};

	enum class MessageType
	{
		ENUM_ENTRY(Message_None)
		ENUM_ENTRY(Message_DeviceAdded)
		ENUM_ENTRY(Message_DeviceRemoved)
		ENUM_ENTRY(Message_BodyFrame)
		ENUM_ENTRY(Message_ExposureFrame)
		ENUM_ENTRY(Message_PixelRead)
		ENUM_ENTRY(Message_LatencyTestSamples)
		ENUM_ENTRY(Message_LatencyTestColorDetected)
		ENUM_ENTRY(Message_LatencyTestStarted)
		ENUM_ENTRY(Message_LatencyTestButton)
		ENUM_ENTRY(Message_CameraFrame)
		ENUM_ENTRY(Message_CameraAdded)
	};

}