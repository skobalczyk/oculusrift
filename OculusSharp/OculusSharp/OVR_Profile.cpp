#include "stdafx.h"
#include "OVR_Device.h"
#include <msclr\marshal_cppstd.h>

namespace OculusSharp {

	// ---------------------------------------------------------
	// --- Profile Manager
	ProfileManager^ ProfileManager::Create() { return gcnew ProfileManager(OVR::ProfileManager::Create()); }
	int ProfileManager::GetUserCount() { return Native()->GetUserCount(); }
	System::String^ ProfileManager::GetUser(unsigned int index) { return gcnew System::String(Native()->GetUser(index)); }
	bool ProfileManager::CreateUser(System::String^ user, System::String^ name) 
	{ 
		std::string userString = msclr::interop::marshal_as<std::string>(user);
		std::string nameString = msclr::interop::marshal_as<std::string>(user);
		return Native()->CreateUser(userString.c_str(), nameString.c_str());
	}
	bool ProfileManager::RemoveUser(System::String^ user)
	{
		std::string userString = msclr::interop::marshal_as<std::string>(user);
		return Native()->RemoveUser(userString.c_str());
	}
	System::String^ ProfileManager::GetDefaultUser(DeviceBase^ device) { return gcnew System::String(Native()->GetDefaultUser(device->Native())); }
	bool ProfileManager::SetDefaultUser(DeviceBase^ device, System::String^ user)
	{
		std::string userString = msclr::interop::marshal_as<std::string>(user);
		return Native()->SetDefaultUser(device->Native(), userString.c_str());
	}
	Profile^ ProfileManager::CreateProfile() { return gcnew Profile(Native()->CreateProfile()); }
	Profile^ ProfileManager::GetProfile(DeviceBase^ device, System::String^ user)
	{
		std::string userString = msclr::interop::marshal_as<std::string>(user);
		return gcnew Profile(Native()->GetProfile(device->Native(), userString.c_str()));
	}
	Profile^ ProfileManager::GetDefaultProfile(DeviceBase^ device) { return gcnew Profile(Native()->GetDefaultProfile(device->Native())); }
	// TODO
	Profile^ ProfileManager::GetTaggedProfile(System::Collections::Generic::Dictionary<System::String^, System::String^>^ keys) { return nullptr; }
	bool ProfileManager::SetTaggedProfile(System::Collections::Generic::Dictionary<System::String^, System::String^>^ keys, Profile^ profile) { return false; }
	bool ProfileManager::GetDeviceTags(DeviceBase^ device, System::String^% product, System::String^% serial) { bool res; OVR::String prod, ser; res = Native()->GetDeviceTags(device->Native(), prod, ser); product = gcnew System::String(prod); serial = gcnew System::String(ser); return res; }

	// ---------------------------------------------------------
	// --- Profile
	int                 Profile::GetNumValues(System::String^ key) { return Native()->GetNumValues(msclr::interop::marshal_as<std::string>(key).c_str()); }
	System::String^     Profile::GetValue(System::String^ key){ return gcnew System::String(Native()->GetValue(msclr::interop::marshal_as<std::string>(key).c_str())); }
	System::String^     Profile::GetValue(System::String^ key, System::String^ val, int val_length){ return gcnew System::String(Native()->GetValue(msclr::interop::marshal_as<std::string>(key).c_str(), (char*)msclr::interop::marshal_as<std::string>(val).c_str(), val_length)); }
	bool                Profile::GetBoolValue(System::String^ key, bool default_val) { return Native()->GetBoolValue(msclr::interop::marshal_as<std::string>(key).c_str(), default_val); }
	int                 Profile::GetIntValue(System::String^ key, int default_val) { return Native()->GetIntValue(msclr::interop::marshal_as<std::string>(key).c_str(), default_val); }
	float               Profile::GetFloatValue(System::String^ key, float default_val) { return Native()->GetFloatValue(msclr::interop::marshal_as<std::string>(key).c_str(), default_val); }
	cli::array<float>^  Profile::GetFloatValues(System::String^ key, int num_vals){ cli::array<float>^ res = gcnew cli::array<float>(num_vals); pin_ptr<float> ptr = &res[0];  Native()->GetFloatValues(msclr::interop::marshal_as<std::string>(key).c_str(), (float*)ptr, num_vals); return res; }
	double              Profile::GetDoubleValue(System::String^ key, double default_val) { return Native()->GetDoubleValue(msclr::interop::marshal_as<std::string>(key).c_str(), default_val); }
	cli::array<double>^ Profile::GetDoubleValues(System::String^ key, int num_vals){ cli::array<double>^ res = gcnew cli::array<double>(num_vals); pin_ptr<double> ptr = &res[0];  Native()->GetDoubleValues(msclr::interop::marshal_as<std::string>(key).c_str(), (double*)ptr, num_vals); return res; }
	void                Profile::SetValue(System::String^ key, System::String^ val) { Native()->SetBoolValue(msclr::interop::marshal_as<std::string>(key).c_str(), msclr::interop::marshal_as<std::string>(val).c_str()); }
	void                Profile::SetBoolValue(System::String^ key, bool val) { Native()->SetBoolValue(msclr::interop::marshal_as<std::string>(key).c_str(), val); }
	void                Profile::SetIntValue(System::String^ key, int val) { Native()->SetIntValue(msclr::interop::marshal_as<std::string>(key).c_str(), val); }
	void                Profile::SetFloatValue(System::String^ key, float val) { Native()->SetFloatValue(msclr::interop::marshal_as<std::string>(key).c_str(), val); }
	void                Profile::SetFloatValues(System::String^ key, cli::array<float>^ vals) { pin_ptr<float> ptr = &vals[0];  Native()->GetFloatValues(msclr::interop::marshal_as<std::string>(key).c_str(), (float*)ptr, vals->Length); }
	void                Profile::SetDoubleValue(System::String^ key, double val) { Native()->SetDoubleValue(msclr::interop::marshal_as<std::string>(key).c_str(), val); }
	void                Profile::SetDoubleValues(System::String^ key, cli::array<double>^ vals) { pin_ptr<double> ptr = &vals[0];  Native()->GetDoubleValues(msclr::interop::marshal_as<std::string>(key).c_str(), (double*)ptr, vals->Length); }
	bool Profile::Close() { return Native()->Close(); }

}
