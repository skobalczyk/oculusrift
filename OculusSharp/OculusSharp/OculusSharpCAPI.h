#pragma once

#define OVR_D3D_VERSION 11
#include "OVR.h"
#include "..\Src\OVR_CAPI.h"
#include "..\Src\OVR_CAPI_D3D.h"

using namespace System;
using namespace System::Runtime::InteropServices;

#define WRAPPER_BEGIN(N,M) \
[StructLayout(LayoutKind::Sequential)] \
public value struct M \
{ \
public: \
		M(N& vi) { pin_ptr<M> ptr = this; *(N*)ptr = vi; } \
		N Native() { N ret; pin_ptr<M> ptr = this; ret = *(N*)ptr; return ret; } \
		void Update(N& vi) { pin_ptr<M> ptr = this; *(N*)ptr = vi; } \
		static operator N& (M^ self) { return self->Native(); } \
		static operator M (N& self) { return M(self); }


#define WRAPPER_END }

#define ENUM_ENTRY(entry) entry = OVR::##entry,
#define ENUM_ENTRYC(entry) entry = ovr##entry,


namespace OculusSharp {
namespace CAPI {

	WRAPPER_BEGIN(ovrVector2i, Vector2i)
		int x, y;
	};

	WRAPPER_BEGIN(ovrSizei, Sizei)
	int w, h;
		};

	WRAPPER_BEGIN(ovrRecti, Recti)
		Vector2i Pos;
		Sizei Size;
	};

	WRAPPER_BEGIN(ovrQuatf, Quatf)
		float x, y, z, w;
	};

	WRAPPER_BEGIN(ovrVector2f, Vector2f)
		float x, y;
	};

	WRAPPER_BEGIN(ovrVector3f, Vector3f)
		float x, y, z;
	};

	WRAPPER_BEGIN(ovrMatrix4f, Matrix4f)
		float M00, M01, M02, M03;
		float M10, M11, M12, M13;
		float M20, M21, M22, M23;
		float M30, M31, M32, M33;
	};

	WRAPPER_BEGIN(ovrPosef, Posef)
		Quatf     Orientation;
		Vector3f  Position;
	};

	WRAPPER_BEGIN(ovrPoseStatef, PoseStatef)
		Posef     Pose;
		Vector3f  AngularVelocity;
		Vector3f  LinearVelocity;
		Vector3f  AngularAcceleration;
		Vector3f  LinearAcceleration;
		double    TimeInSeconds;
	};

	WRAPPER_BEGIN(ovrFovPort, FovPort)
		float UpTan;
		float DownTan;
		float LeftTan;
		float RightTan;
	};

	public enum class HmdType
	{
		ENUM_ENTRYC(Hmd_None)
		ENUM_ENTRYC(Hmd_DK1)
		ENUM_ENTRYC(Hmd_DKHD)
		ENUM_ENTRYC(Hmd_DK2)
		ENUM_ENTRYC(Hmd_Other)
	};

	[FlagsAttribute]
	public enum class HmdCap
	{
		ENUM_ENTRYC(HmdCap_Present)
		ENUM_ENTRYC(HmdCap_Available)
		ENUM_ENTRYC(HmdCap_Captured)
		ENUM_ENTRYC(HmdCap_ExtendDesktop)
		ENUM_ENTRYC(HmdCap_NoMirrorToWindow)
		ENUM_ENTRYC(HmdCap_DisplayOff)
		ENUM_ENTRYC(HmdCap_LowPersistence)
		ENUM_ENTRYC(HmdCap_DynamicPrediction)
		ENUM_ENTRYC(HmdCap_NoVSync)
		ENUM_ENTRYC(HmdCap_Writable_Mask)
		ENUM_ENTRYC(HmdCap_Service_Mask)
	};


	[FlagsAttribute]
	public enum class TrackingCap
	{
		ENUM_ENTRYC(TrackingCap_Orientation)
		ENUM_ENTRYC(TrackingCap_MagYawCorrection)
		ENUM_ENTRYC(TrackingCap_Position)
		ENUM_ENTRYC(TrackingCap_Idle)
	};

	[FlagsAttribute]
	public enum class DistortionCap
	{
		ENUM_ENTRYC(DistortionCap_Chromatic)
		ENUM_ENTRYC(DistortionCap_TimeWarp)
		ENUM_ENTRYC(DistortionCap_Vignette)
		ENUM_ENTRYC(DistortionCap_NoRestore)
		ENUM_ENTRYC(DistortionCap_FlipInput)
		ENUM_ENTRYC(DistortionCap_SRGB)
		ENUM_ENTRYC(DistortionCap_Overdrive)
		ENUM_ENTRYC(DistortionCap_ProfileNoTimewarpSpinWaits)
	};


	public enum class EyeType
	{
		ENUM_ENTRYC(Eye_Left)
		ENUM_ENTRYC(Eye_Right)
		ENUM_ENTRYC(Eye_Count)
	};


	// This is a complete descriptor of the HMD.
	public value struct HmdDesc
	{
	public:
		HmdDesc(ovrHmdDesc& vi)
		{
			Handle = (IntPtr)vi.Handle;
			Type = (HmdType)vi.Type;
			ProductName = gcnew String(vi.ProductName);
			Manufacturer = gcnew String(vi.Manufacturer);
			VendorId = vi.VendorId;
			ProductId = vi.ProductId;
			SerialNumber = gcnew System::String(vi.SerialNumber);
			FirmwareMajor = vi.FirmwareMajor;
			FirmwareMinor = vi.FirmwareMinor;
			CameraFrustumHFovInRadians = vi.CameraFrustumHFovInRadians;
			CameraFrustumVFovInRadians = vi.CameraFrustumVFovInRadians;
			CameraFrustumNearZInMeters = vi.CameraFrustumNearZInMeters;
			CameraFrustumFarZInMeters = vi.CameraFrustumFarZInMeters;
			HmdCaps = (HmdCap)vi.HmdCaps;
			TrackingCaps = (TrackingCap)vi.TrackingCaps;
			DistortionCaps = (DistortionCap)vi.DistortionCaps;
			Resolution = vi.Resolution;
			WindowsPos = vi.WindowsPos;
			DefaultEyeFov = gcnew cli::array<FovPort>(2);
			MaxEyeFov = gcnew cli::array<FovPort>(2);
			EyeRenderOrder = gcnew cli::array<EyeType>(2);
			for (int i = 0; i < (int)EyeType::Eye_Count; i++)
			{
				DefaultEyeFov[i] = vi.DefaultEyeFov[i];
				MaxEyeFov[i] = vi.MaxEyeFov[i];
				EyeRenderOrder[i] = (EyeType)vi.EyeRenderOrder[i];
			}
			DisplayDeviceName = gcnew String(vi.DisplayDeviceName);
			DisplayId = vi.DisplayId;
		}

		IntPtr Handle;
		HmdType  Type;
		System::String^ ProductName;
		System::String^ Manufacturer;
		Int16 VendorId;
		Int16 ProductId;
		System::String^ SerialNumber;
		Int16 FirmwareMajor;
		Int16 FirmwareMinor;
		float CameraFrustumHFovInRadians;
		float CameraFrustumVFovInRadians;
		float CameraFrustumNearZInMeters;
		float CameraFrustumFarZInMeters;
		HmdCap HmdCaps;
		TrackingCap TrackingCaps;
		DistortionCap DistortionCaps;
		cli::array<FovPort>^ DefaultEyeFov;
		cli::array<FovPort>^ MaxEyeFov;
		cli::array<EyeType>^ EyeRenderOrder;
		Sizei    Resolution;
		Vector2i WindowsPos;
		System::String^ DisplayDeviceName;
		int DisplayId;
	};

	// Bit flags describing the current status of sensor tracking.
	public enum class Status
	{
		ENUM_ENTRYC(Status_OrientationTracked)
		ENUM_ENTRYC(Status_PositionTracked)
		ENUM_ENTRYC(Status_CameraPoseTracked)
		ENUM_ENTRYC(Status_PositionConnected)
		ENUM_ENTRYC(Status_HmdConnected)
	};

	WRAPPER_BEGIN(ovrSensorData, SensorData)
		Vector3f Accelerometer;
		Vector3f Gyro;
		Vector3f Magnetometer;
		float Temperature;
		float TimeInSeconds;
	};

	
	WRAPPER_BEGIN(ovrTrackingState, TrackingState)
		PoseStatef HeadPose;
		Posef CameraPose;
		Posef LeveledCameraPose;
		SensorData RawSensorData;
		unsigned int   StatusFlags;
	};

	WRAPPER_BEGIN(ovrFrameTiming, FrameTiming)
		float DeltaSeconds;
		double ThisFrameSeconds;
		double TimewarpPointSeconds;
		double NextFrameSeconds;
		double ScanoutMidpointSeconds;
		cli::array<double>^ EyeScanoutSeconds;
	};

	WRAPPER_BEGIN(ovrEyeRenderDesc, EyeRenderDesc)
		EyeType  Eye;
		FovPort  Fov;
		Recti	DistortedViewport;
		Vector2f PixelsPerTanAngleAtCenter;
		Vector3f ViewAdjust;
	};

	public enum class RenderAPIType
	{
		ENUM_ENTRYC(RenderAPI_None)
		ENUM_ENTRYC(RenderAPI_OpenGL)
		ENUM_ENTRYC(RenderAPI_Android_GLES)
		ENUM_ENTRYC(RenderAPI_D3D9)
		ENUM_ENTRYC(RenderAPI_D3D10)
		ENUM_ENTRYC(RenderAPI_D3D11)
		ENUM_ENTRYC(RenderAPI_Count)
	};

	WRAPPER_BEGIN(ovrHSWDisplayState, HSWDisplayState)
		bool Displayed;
		double StartTime;
		double DismissibleTime;
	};

	WRAPPER_BEGIN(ovrRenderAPIConfigHeader, RenderAPIConfigHeader)
		RenderAPIType API;
		Sizei         RTSize;
		int           Multisample;
	};

	WRAPPER_BEGIN(ovrRenderAPIConfig, RenderAPIConfig)
		RenderAPIConfigHeader Header;
		uintptr_t PlatformData0;
		uintptr_t PlatformData1;
		uintptr_t PlatformData2;
		uintptr_t PlatformData3;
		uintptr_t PlatformData4;
		uintptr_t PlatformData5;
		uintptr_t PlatformData6;
		uintptr_t PlatformData7;
	};

	WRAPPER_BEGIN(ovrTextureHeader, TextureHeader)
		RenderAPIType API;
		Sizei         TextureSize;
		Recti         RenderViewport;
	};

	WRAPPER_BEGIN(ovrTexture, Texture)
		TextureHeader Header;
		uintptr_t PlatformData0;
		uintptr_t PlatformData1;
		uintptr_t PlatformData2;
		uintptr_t PlatformData3;
		uintptr_t PlatformData4;
		uintptr_t PlatformData5;
		uintptr_t PlatformData6;
		uintptr_t PlatformData7;
	};


	WRAPPER_BEGIN(ovrDistortionVertex, DistortionVertex)
		Vector2f ScreenPosNDC;
		float TimeWarpFactor;
		float VignetteFactor;
		Vector2f TanEyeAnglesR;
		Vector2f TanEyeAnglesG;
		Vector2f TanEyeAnglesB;
	};

	public ref class DistortionMesh
	{
	public:
		System::Collections::Generic::List<DistortionVertex>^ VertexData;
		System::Collections::Generic::List<unsigned short>^ IndexData;
		unsigned int         VertexCount;
		unsigned int         IndexCount;
	};

	WRAPPER_BEGIN(ovrD3D11ConfigData, D3D11ConfigData)
		RenderAPIConfigHeader	Header;
		System::IntPtr			pDevice;
		System::IntPtr			pDeviceContext;
		System::IntPtr			pBackBufferRT;
		System::IntPtr			pSwapChain;
		uintptr_t				_pad1;
		uintptr_t				_pad2;
		uintptr_t				_pad3;
		uintptr_t				_pad4;
	};

	WRAPPER_BEGIN(ovrD3D11TextureData, D3D11TextureData)
		TextureHeader			Header;
		System::IntPtr			pTexture;
		System::IntPtr			pSRView;
		uintptr_t				_pad1;
		uintptr_t				_pad2;
		uintptr_t				_pad3;
		uintptr_t				_pad4;
		uintptr_t				_pad5;
		uintptr_t				_pad6;
	};

	public ref class Hmd
	{
	private:
		ovrHmd	deviceHandle;
	public:
		Hmd(HmdType type)
		{
			deviceHandle = ovrHmd_CreateDebug((ovrHmdType)type);
		}
		Hmd(int index)
		{
			deviceHandle = ovrHmd_Create(index);
		}
		~Hmd()
		{
			if (deviceHandle) ovrHmd_Destroy(deviceHandle);
			deviceHandle = NULL;
		}
		!Hmd()
		{
			if (deviceHandle) ovrHmd_Destroy(deviceHandle);
			deviceHandle = NULL;
		}

	public:
		static Matrix4f Matrix4f_Projection(FovPort fov, float znear, float zfar, bool rightHanded);
		static Matrix4f Matrix4f_OrthoSubProjection(Matrix4f projection, Vector2f orthoScale, float orthoDistance, float eyeViewAdjustX);
		static double   GetTimeInSeconds();
		static double   WaitTillTime(double absTime);

		// Init Functions
	public:
		static bool Initialize();
		static void Shutdown();
		System::String^ GetVersionString();
		static int DetectHmd();
		System::String^ GetLastError();
		bool AttachToWindow(IntPtr window, Recti^ destMirrorRect, Recti^ sourceRenderTargetRect);
		HmdCap GetEnabledCaps();
		void SetEnabledCaps(HmdCap hmdCaps);
		HmdDesc GetDesc();

		// Tracking
	public:
		bool ConfigureTracking(TrackingCap supportedTrackingCaps, TrackingCap requiredTrackingCaps);
		void RecenterPose();
		TrackingState GetTrackingState(double absTime);

		// Graphics setup
	public:
		Sizei GetFovTextureSize(EyeType eye, FovPort fov, float pixelsPerDisplayPixel);

		// Distortion rendering
	public:
		bool ConfigureRendering(D3D11ConfigData apiConfig, DistortionCap distortionCaps, System::Collections::Generic::List<FovPort>^ eyeFovIn, System::Collections::Generic::List<EyeRenderDesc>^ eyeRenderDescOut);
		FrameTiming BeginFrame(unsigned int frameIndex);
		void EndFrame(cli::array<Posef>^ pose, cli::array<Texture>^ texture);
		void EndFrame(cli::array<Posef>^ pose, cli::array<D3D11TextureData>^ texture);
		Posef GetEyePose(EyeType eye);

		// Client Distortion
	public:
		EyeRenderDesc GetRenderDesc(EyeType eyeType, FovPort fov);
		bool CreateDistortionMesh(EyeType eyeType, FovPort fov, DistortionCap distortionCaps, DistortionMesh^ meshData);
		void GetRenderScaleAndOffset(FovPort fov, Sizei textureSize, Recti renderViewport, System::Collections::Generic::List<Vector2f>^ uvScaleOffsetOut);
		FrameTiming GetFrameTiming(unsigned int frameIndex);
		FrameTiming BeginFrameTiming(unsigned int frameIndex);
		void EndFrameTiming();
		void ResetFrameTiming(unsigned int frameIndex);
		void GetEyeTimewarpMatrices(EyeType eye, Posef renderPose, System::Collections::Generic::List<Matrix4f>^ twmOut);

		// Latency Testing
	public:
		bool ProcessLatencyTest(System::Drawing::Color^ outColor);
		System::String^ GetLatencyTestResult();

		// Health and Sefety
	public:
		void GetHSWDisplayState(HSWDisplayState% hasWarningState);
		bool DismissHSWDisplay();

		// Property Access
	public:
		bool GetBool(System::String^ propertyName, bool defaultVal);
		bool SetBool(System::String^ propertyName, bool value);

		int GetInt(System::String^ propertyName, int defaultVal);
		bool SetInt(System::String^ propertyName, int value);

		float GetFloat(System::String^ propertyName, float defaultVal);
		bool SetFloat(System::String^ propertyName, float value);

		unsigned int GetFloatArray(System::String^ propertyName, cli::array<float>^ values);
		bool SetFloatArray(System::String^ propertyName, cli::array<float>^ values);

		System::String^ GetString(System::String^ propertyName, System::String^ defaultVal);
		bool SetString(System::String^ propertyName, System::String^ value);
	};
}
}
