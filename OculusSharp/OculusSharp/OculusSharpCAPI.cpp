// This is the main DLL file.

#include "stdafx.h"

#include "OculusSharpCAPI.h"
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>

using namespace msclr::interop;

namespace OculusSharp {
	namespace CAPI {


		bool Hmd::Initialize()
		{
			return ovr_Initialize() != 0 ? true : false;
		}

		void Hmd::Shutdown()
		{
			ovr_Shutdown();
		}

		int Hmd::DetectHmd()
		{
			return ovrHmd_Detect();
		}

		Matrix4f Hmd::Matrix4f_Projection(FovPort fov, float znear, float zfar, bool rightHanded)
		{
			return ovrMatrix4f_Projection(fov.Native(), znear, zfar, rightHanded);
		}

		Matrix4f Hmd::Matrix4f_OrthoSubProjection(Matrix4f projection, Vector2f orthoScale, float orthoDistance, float eyeViewAdjustX)
		{
			return ovrMatrix4f_OrthoSubProjection(projection.Native(), orthoScale.Native(), orthoDistance, eyeViewAdjustX);
		}

		double Hmd::GetTimeInSeconds()
		{
			return ovr_GetTimeInSeconds();
		}

		double Hmd::WaitTillTime(double absTime)
		{
			return ovr_WaitTillTime(absTime);
		}

		void Hmd::GetHSWDisplayState(HSWDisplayState% hasWarningState)
		{
			ovrHSWDisplayState state;
			ovrHmd_GetHSWDisplayState(deviceHandle, &state);
			hasWarningState = HSWDisplayState(state);
		}

		bool Hmd::DismissHSWDisplay()
		{
			return ovrHmd_DismissHSWDisplay(deviceHandle);
		}

		System::String^ Hmd::GetLastError()
		{
			return gcnew System::String(ovrHmd_GetLastError(deviceHandle));
		}

		bool Hmd::AttachToWindow(IntPtr window, Recti^ destMirrorRect, Recti^ sourceRenderTargetRect)
		{
			return ovrHmd_AttachToWindow(deviceHandle, (void*)window, destMirrorRect == nullptr ? nullptr : &destMirrorRect->Native(), sourceRenderTargetRect == nullptr ? nullptr : &sourceRenderTargetRect->Native());
		}

		System::String^ Hmd::GetVersionString()
		{
			return gcnew System::String(ovr_GetVersionString());
		}

		HmdCap Hmd::GetEnabledCaps()
		{
			return (HmdCap)ovrHmd_GetEnabledCaps(deviceHandle);
		}

		HmdDesc Hmd::GetDesc()
		{
			return HmdDesc(*(ovrHmdDesc*)deviceHandle);
		}

		void Hmd::SetEnabledCaps(HmdCap hmdCaps)
		{
			ovrHmd_SetEnabledCaps(deviceHandle, (ovrHmdCaps)hmdCaps);
		}

		bool Hmd::ConfigureTracking(TrackingCap supportedTrackingCaps, TrackingCap requiredTrackingCaps)
		{
			return ovrHmd_ConfigureTracking(deviceHandle, (unsigned int)supportedTrackingCaps, (unsigned int)requiredTrackingCaps);
		}

		void Hmd::RecenterPose()
		{
			ovrHmd_RecenterPose(deviceHandle);
		}

		TrackingState Hmd::GetTrackingState(double absTime)
		{
			return TrackingState(ovrHmd_GetTrackingState(deviceHandle, absTime));
		}


		Sizei Hmd::GetFovTextureSize(EyeType eye, FovPort fov, float pixelsPerDisplayPixel)
		{
			return ovrHmd_GetFovTextureSize(deviceHandle, (ovrEyeType)eye, fov.Native(), pixelsPerDisplayPixel);
		}

		bool Hmd::ConfigureRendering(D3D11ConfigData apiConfig, DistortionCap distortionCaps, System::Collections::Generic::List<FovPort>^ eyeFovIn, System::Collections::Generic::List<EyeRenderDesc>^ eyeRenderDescOut)
		{
			ovrFovPort _eyeFovIn[2];
			ovrEyeRenderDesc _eyeRenderDesc[2];
			ovrD3D11ConfigData config = apiConfig.Native();
			_eyeFovIn[0] = eyeFovIn[0].Native();
			_eyeFovIn[1] = eyeFovIn[1].Native();
			bool res = ovrHmd_ConfigureRendering(deviceHandle, (ovrRenderAPIConfig*)&config, (ovrDistortionCaps)distortionCaps, _eyeFovIn, _eyeRenderDesc) != 0 ? true : false;
			eyeRenderDescOut->Add(_eyeRenderDesc[0]);
			eyeRenderDescOut->Add(_eyeRenderDesc[1]);
			return res;
		}

		FrameTiming Hmd::BeginFrame(unsigned int frameIndex)
		{
			return ovrHmd_BeginFrame(deviceHandle, frameIndex);
		}

		void Hmd::EndFrame(cli::array<Posef>^ pose, cli::array<Texture>^ texture)
		{
			ovrPosef poseNative[2];
			poseNative[0] = pose[0].Native();
			poseNative[1] = pose[1].Native();
			ovrTexture textureNative[2];
			textureNative[0] = texture[0].Native();
			textureNative[1] = texture[1].Native();
			return ovrHmd_EndFrame(deviceHandle, (ovrPosef*)poseNative, (ovrTexture*)textureNative);
		}

		void Hmd::EndFrame(cli::array<Posef>^ pose, cli::array<D3D11TextureData>^ texture)
		{
			ovrPosef poseNative[2];
			poseNative[0] = pose[0].Native();
			poseNative[1] = pose[1].Native();
			ovrTexture textureNative[2];
			textureNative[0] = *(ovrTexture*)&texture[0].Native();
			textureNative[1] = *(ovrTexture*)&texture[1].Native();
			return ovrHmd_EndFrame(deviceHandle, (ovrPosef*)poseNative, (ovrTexture*)textureNative);
		}

		EyeRenderDesc Hmd::GetRenderDesc(EyeType eyeType, FovPort fov)
		{
			return ovrHmd_GetRenderDesc(deviceHandle, (ovrEyeType)eyeType, fov.Native());
		}

		bool Hmd::CreateDistortionMesh(EyeType eyeType, FovPort fov, DistortionCap distortionCaps, DistortionMesh^ meshData)
		{
			ovrDistortionMesh mesh;
			bool res = ovrHmd_CreateDistortionMesh(deviceHandle, (ovrEyeType)eyeType, fov.Native(), (ovrDistortionCaps)distortionCaps, &mesh) != 0 ? true : false;

			meshData->IndexCount = mesh.IndexCount;
			meshData->IndexData = gcnew System::Collections::Generic::List<unsigned short>(meshData->IndexCount);
			for (unsigned int idx = 0; idx < meshData->IndexCount; idx++) 
				meshData->IndexData->Add(mesh.pIndexData[idx]);

			meshData->VertexCount = mesh.VertexCount;
			meshData->VertexData = gcnew System::Collections::Generic::List<DistortionVertex>(meshData->VertexCount);
			for (unsigned int idx = 0; idx < meshData->VertexCount; idx++) 
				meshData->VertexData->Add(mesh.pVertexData[idx]);

			ovrHmd_DestroyDistortionMesh(&mesh);

			return res;
		}

		void Hmd::GetRenderScaleAndOffset(FovPort fov, Sizei textureSize, Recti renderViewport, System::Collections::Generic::List<Vector2f>^ uvScaleOffsetOut)
		{
			ovrVector2f uvScale[2];
			ovrHmd_GetRenderScaleAndOffset(fov.Native(), textureSize.Native(), renderViewport.Native(), uvScale);
			uvScaleOffsetOut->Add(uvScale[0]);
			uvScaleOffsetOut->Add(uvScale[1]);
		}

		FrameTiming Hmd::GetFrameTiming(unsigned int frameIndex)
		{
			return ovrHmd_GetFrameTiming(deviceHandle, frameIndex);
		}

		FrameTiming Hmd::BeginFrameTiming(unsigned int frameIndex)
		{
			return ovrHmd_BeginFrameTiming(deviceHandle, frameIndex);
		}

		void Hmd::EndFrameTiming()
		{
			ovrHmd_EndFrameTiming(deviceHandle);
		}

		void Hmd::ResetFrameTiming(unsigned int frameIndex)
		{
			return ovrHmd_ResetFrameTiming(deviceHandle, frameIndex);
		}

		Posef Hmd::GetEyePose(EyeType eye)
		{
			return ovrHmd_GetEyePose(deviceHandle, (ovrEyeType)eye);
		}

		void Hmd::GetEyeTimewarpMatrices(EyeType eye, Posef renderPose, System::Collections::Generic::List<Matrix4f>^ twmOut)
		{
			ovrMatrix4f twm[2];
			ovrHmd_GetEyeTimewarpMatrices(deviceHandle, (ovrEyeType)eye, renderPose.Native(), twm);
			twmOut->Add(twm[0]);
			twmOut->Add(twm[1]);
		}

		bool Hmd::ProcessLatencyTest(System::Drawing::Color^ outColor)
		{
			byte col[3];
			bool res = ovrHmd_ProcessLatencyTest(deviceHandle, col);
			outColor = System::Drawing::Color::FromArgb(col[0], col[1], col[2]);
			return res;
		}

		System::String^ Hmd::GetLatencyTestResult()
		{
			return gcnew System::String(ovrHmd_GetLatencyTestResult(deviceHandle));
		}

		bool Hmd::GetBool(System::String^ propertyName, bool defaultVal)
		{
			return ovrHmd_GetBool(deviceHandle, marshal_as<std::string>(propertyName).c_str(), defaultVal);
		}

		bool Hmd::SetBool(System::String^ propertyName, bool value)
		{
			return ovrHmd_SetBool(deviceHandle, marshal_as<std::string>(propertyName).c_str(), value);
		}

		int Hmd::GetInt(System::String^ propertyName, int defaultVal)
		{
			return ovrHmd_GetInt(deviceHandle, marshal_as<std::string>(propertyName).c_str(), defaultVal);
		}
		
		bool Hmd::SetInt(System::String^ propertyName, int value)
		{
			return ovrHmd_SetInt(deviceHandle, marshal_as<std::string>(propertyName).c_str(), value);
		}

		float Hmd::GetFloat(System::String^ propertyName, float defaultVal)
		{
			return ovrHmd_GetFloat(deviceHandle, marshal_as<std::string>(propertyName).c_str(), defaultVal);
		}

		bool Hmd::SetFloat(System::String^ propertyName, float value)
		{
			return ovrHmd_SetFloat(deviceHandle, marshal_as<std::string>(propertyName).c_str(), value);
		}

		unsigned int Hmd::GetFloatArray(System::String^ propertyName, cli::array<float>^ values)
		{
			pin_ptr<float> v = &values[0];
			return ovrHmd_GetFloatArray(deviceHandle, marshal_as<std::string>(propertyName).c_str(), v, values->Length);
		}

		bool Hmd::SetFloatArray(System::String^ propertyName, cli::array<float>^ values)
		{
			pin_ptr<float> v = &values[0];
			return ovrHmd_SetFloatArray(deviceHandle, marshal_as<std::string>(propertyName).c_str(), v, values->Length);
		}

		System::String^ Hmd::GetString(System::String^ propertyName, System::String^ defaultVal)
		{
			return gcnew System::String(ovrHmd_GetString(deviceHandle, marshal_as<std::string>(propertyName).c_str(), marshal_as<std::string>(defaultVal).c_str()));
		}

		bool Hmd::SetString(System::String^ propertyName, System::String^ value)
		{
			return ovrHmd_SetString(deviceHandle, marshal_as<std::string>(propertyName).c_str(), marshal_as<std::string>(value).c_str());
		}
}
}
